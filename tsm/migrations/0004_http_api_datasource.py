# Generated by Django 4.2.11 on 2024-06-04 10:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("tsm", "0003_alter_csvparser_delimiter"),
    ]

    operations = [
        migrations.CreateModel(
            name="NmStation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "station_id",
                    models.CharField(
                        max_length=5, unique=True, verbose_name="Station ID"
                    ),
                ),
                (
                    "description",
                    models.CharField(max_length=255, verbose_name="Description"),
                ),
            ],
        ),
        migrations.AddField(
            model_name="thing",
            name="http_api_type",
            field=models.CharField(
                blank=True,
                choices=[
                    (None, "Select an external API service"),
                    ("ttn", "The Things Network (TTN)"),
                    ("dwd", "Deutscher Wetterdienst (DWD)"),
                    ("nm", "Neutronmonitor (NM)"),
                ],
                max_length=8,
                null=True,
                verbose_name="External API Service",
            ),
        ),
        migrations.AlterField(
            model_name="thing",
            name="datasource_type",
            field=models.CharField(
                choices=[
                    ("MQTT", "MQTT"),
                    ("SFTP", "SFTP"),
                    ("extSFTP", "External SFTP"),
                    ("extAPI", "External API service"),
                ],
                default="SFTP",
                max_length=8,
                verbose_name="Ingest Type",
            ),
        ),
        migrations.CreateModel(
            name="TtnApi",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "sync_enabled",
                    models.BooleanField(
                        blank=True, null=True, verbose_name="Sync enabled"
                    ),
                ),
                (
                    "sync_interval",
                    models.IntegerField(
                        blank=True, null=True, verbose_name="Sync Interval"
                    ),
                ),
                (
                    "api_key",
                    models.CharField(
                        blank=True, max_length=400, null=True, verbose_name="API-Key"
                    ),
                ),
                (
                    "endpoint_uri",
                    models.CharField(
                        blank=True,
                        max_length=400,
                        null=True,
                        verbose_name="Endpoint-URI",
                    ),
                ),
                (
                    "thing",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="tsm.thing"
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="NmApi",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "sync_enabled",
                    models.BooleanField(
                        blank=True, null=True, verbose_name="Sync enabled"
                    ),
                ),
                (
                    "sync_interval",
                    models.IntegerField(
                        blank=True, null=True, verbose_name="Sync Interval"
                    ),
                ),
                (
                    "time_resolution",
                    models.IntegerField(
                        blank=True,
                        choices=[
                            (0, "0 min"),
                            (2, "2 min"),
                            (5, "5 min"),
                            (10, "10 min"),
                            (30, "30 min"),
                            (60, "1 h"),
                            (120, "2 h"),
                            (360, "6 h"),
                            (720, "12 h"),
                            (1440, "1 d"),
                            (39276, "1 mo"),
                            (525969, "1 y"),
                        ],
                        default=60,
                        null=True,
                        verbose_name="Time resolution",
                    ),
                ),
                (
                    "station",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="tsm.nmstation",
                    ),
                ),
                (
                    "thing",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="tsm.thing"
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="DwdApi",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "sync_enabled",
                    models.BooleanField(
                        blank=True, null=True, verbose_name="Sync enabled"
                    ),
                ),
                (
                    "station_id",
                    models.CharField(max_length=5, verbose_name="Station ID"),
                ),
                (
                    "sync_interval",
                    models.IntegerField(default=1440, verbose_name="Sync Interval"),
                ),
                (
                    "thing",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="tsm.thing"
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
