from django.db import migrations, models
import django.db.models.deletion


def migrate_timestamps(apps, schema_editor):
    csv_parser = apps.get_model("tsm", "CsvParser")
    timestamp_column = apps.get_model("tsm", "TimestampColumn")

    for parser in csv_parser.objects.all():
        if parser.timestamp_column is not None and parser.timestamp_format:
            timestamp_column.objects.create(
                csv_parser=parser,
                column=parser.timestamp_column,
                format=parser.timestamp_format,
            )


class Migration(migrations.Migration):
    dependencies = [
        ("tsm", "0008_qaqcsetting_active_state"),
    ]

    operations = [
        migrations.CreateModel(
            name="TimestampColumn",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("column", models.PositiveIntegerField()),
                ("format", models.CharField(max_length=200)),
                (
                    "csv_parser",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="timestamp_columns",
                        to="tsm.csvparser",
                    ),
                ),
            ],
        ),
        migrations.RunPython(migrate_timestamps, migrations.RunPython.noop),
        migrations.RemoveField(
            model_name="csvparser",
            name="timestamp_column",
        ),
        migrations.RemoveField(
            model_name="csvparser",
            name="timestamp_format",
        ),
    ]
