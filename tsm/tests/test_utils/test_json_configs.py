import os
import datetime

from tsm.models import (
    Thing,
    MqttDeviceType,
    DwdApi,
    TtnApi,
    NmApi,
    BoschIotApi,
    HttpApi,
    QaqcSetting,
)
from tsm.tests.TestHelpers import TestHelpers
from tsm.utils import (
    get_db_by_thing,
    get_storage_by_thing,
    get_group_name_with_vo_prefix,
    get_parser_properties,
    get_qaqc_properties,
    get_thing_properties,
    get_http_api_properties,
    get_ssh_privkey,
    get_qaqc_setting_properties,
    get_qaqc_setting_data_parsed_properties,
)

import uuid


def mock_encrypt_pw(pw):
    return "[ENCRYPTED]" + pw if pw else None


class JsonConfigTest(TestHelpers.TestCaseWithBaseModelSetup):
    def test_get_parser_properties(self):
        parser_props = get_parser_properties(self.thing)
        expected_json = {
            "default": 0,
            "parsers": [
                {
                    "type": "CsvParser",
                    "name": self.csv_parser.name,
                    "settings": self.csv_parser.get_properties_json(),
                }
            ],
        }
        self.assertDictEqual(parser_props, expected_json)

    def test_get_http_api_properties(self):
        self.thing.http_api_type = DwdApi.get_type_name()
        http_api_props = get_http_api_properties(self.thing)
        self.assertIn("type", http_api_props)
        self.assertIn("settings", http_api_props)
        self.assertIn("sync_interval", http_api_props)
        self.assertIn("enabled", http_api_props)

    def test_get_ext_sftp_properties(self):
        pass

    def test_get_qaqc_properties(self):
        qaqc_props = get_qaqc_properties(self.thing)
        expected_json = {
            "default": 0,
            "configs": [
                {
                    "version": 1,
                    "context_window": self.thing.qaqc_ctx_window,
                    "name": "MyConfig",
                    "tests": self.thing.qaqc_tests if self.thing.qaqc_tests else [],
                    "type": "SaQC",
                }
            ],
        }
        self.assertDictEqual(qaqc_props, expected_json)

    def get_expected_json_by_thing(self, thing):
        database = get_db_by_thing(thing)
        return {
            "version": 6,
            "uuid": str(thing.thing_id),
            "name": thing.name,
            "description": thing.description,
            "database": {
                "username": database.username,
                "password": mock_encrypt_pw(database.password),
                "url": f"postgresql://{database.username}@{database.url}/{database.name}",
                "ro_username": database.ro_username,
                "ro_password": mock_encrypt_pw(database.ro_password),
                "ro_url": f"postgresql://{database.ro_username}@{database.url}/{database.name}",
                "schema": database.username,
            },
            "project": {
                "name": get_group_name_with_vo_prefix(thing.group),
                "uuid": str(uuid.UUID(int=thing.group.id)),
            },
            "ingest_type": thing.datasource_type,
            "qaqc": get_qaqc_properties(thing),
            "parsers": get_parser_properties(thing),
            "mqtt_device_type": (
                thing.mqtt_device_type.name if thing.mqtt_device_type else None
            ),
            "raw_data_storage": {
                "bucket_name": get_storage_by_thing(thing).bucket,
                "filename_pattern": thing.sftp_filename_pattern,
                "username": get_storage_by_thing(thing).access_key,
                "password": mock_encrypt_pw(get_storage_by_thing(thing).secret_key),
            },
            "mqtt": {
                "topic": thing.mqtt_topic,
                "password_hash": thing.mqtt_hashed_password,
                "password": mock_encrypt_pw(thing.mqtt_password),
                "uri": thing.mqtt_uri,
                "username": thing.mqtt_username,
            },
            "external_sftp": {
                "sync_enabled": thing.ext_sftp_sync_enabled,
                "uri": thing.ext_sftp_uri,
                "path": thing.ext_sftp_path,
                "username": thing.ext_sftp_username,
                "password": mock_encrypt_pw(thing.ext_sftp_password),
                "sync_interval": thing.ext_sftp_sync_interval,
                "public_key": thing.ext_sftp_public_key,
                "private_key": mock_encrypt_pw(
                    get_ssh_privkey(f"sftp-private-keys/{thing.thing_id}")
                ),
            },
            "external_api": get_http_api_properties(thing),
        }

    def test_sftp_thing_published_json_config(self):
        sftp_thing = Thing.objects.create(
            name="SFTP_Thing",
            datasource_type="SFTP",
            group=self.group,
            description="Sample description",
            sftp_filename_pattern="*.txt",
            mqtt_uri="mqtt://broker.example.com",
            mqtt_username="mqtt_user",
            mqtt_password="mqtt_password",
            mqtt_topic="sample_topic",
            mqtt_device_type=MqttDeviceType.objects.create(),
            qaqc_ctx_window="window_config",
            qaqc_tests=[{"test_key": "test_value"}],
        )
        sftp_thing.parser.add(self.parser)

        expected_json = self.get_expected_json_by_thing(sftp_thing)
        self.assertDictEqual(get_thing_properties(sftp_thing), expected_json)

    def test_ext_sftp_thing_published_json_config(self):
        ext_sftp_thing = Thing.objects.create(
            name="extSFTP_Thing",
            datasource_type="extSFTP",
            group=self.group,
            description="Sample description",
            sftp_filename_pattern="*.txt",
            mqtt_uri="mqtt://broker.example.com",
            mqtt_username="mqtt_user",
            mqtt_password="mqtt_password",
            mqtt_topic="sample_topic",
            mqtt_device_type=MqttDeviceType.objects.create(),
            ext_sftp_path="/some/path",
            ext_sftp_uri="some.uri",
            ext_sftp_password="password",
            ext_sftp_username="username",
            ext_sftp_sync_interval=42,
            ext_sftp_sync_enabled=True,
            qaqc_ctx_window="window_config",
            qaqc_tests=[{"test_key": "test_value"}],
        )
        ext_sftp_thing.parser.add(self.parser)

        expected_json = self.get_expected_json_by_thing(ext_sftp_thing)
        self.assertDictEqual(get_thing_properties(ext_sftp_thing), expected_json)

    def test_mqtt_thing_published_json_config(self):
        mqtt_thing = Thing.objects.create(
            name="MQTT_Thing",
            datasource_type="MQTT",
            group=self.group,
            description="Sample description",
            sftp_filename_pattern="*.txt",
            mqtt_uri="mqtt://broker.example.com",
            mqtt_username="mqtt_user",
            mqtt_password="mqtt_password",
            mqtt_hashed_password="mqtt_hashed_password",
            mqtt_topic="sample_topic",
            mqtt_device_type=MqttDeviceType.objects.create(),
            qaqc_ctx_window="window_config",
            qaqc_tests=[{"test_key": "test_value"}],
        )

        expected_json = self.get_expected_json_by_thing(mqtt_thing)
        self.assertDictEqual(get_thing_properties(mqtt_thing), expected_json)

    def test_qaqc_setting_published_json_config(self):
        qaqc_setting = QaqcSetting.objects.create(
            context_window="test_context_window",
            name="qaqc_setting",
            active=True,
            group=self.group,
        )
        actual_json = get_qaqc_setting_properties(qaqc_setting)

        expected_json = {
            "version": 3,
            "saqc_version": os.environ.get("QAQC_SETTING_SAQC_VERSION", None),
            "default": True,
            "project_uuid": str(uuid.UUID(int=qaqc_setting.group.id)),
            "id": str(qaqc_setting.id),
            "name": "qaqc_setting",
            "context_window": "test_context_window",
            "functions": [],  # TODO: test separately
        }

        self.assertDictEqual(actual_json, expected_json)

    def test_qaqc_setting_data_parsed_config(self):
        begin_datetime = datetime.datetime.fromisoformat("2022-01-01T00:00:00+00:00")
        end_datetime = datetime.datetime.fromisoformat("2023-01-01T00:00:00+00:00")
        actual_json = get_qaqc_setting_data_parsed_properties(
            self.qaqc_setting, begin_datetime, end_datetime
        )
        expected_json = {
            "version": 2,
            "project_uuid": str(uuid.UUID(int=self.qaqc_setting.group.id)),
            "qc_settings_name": self.qaqc_setting.name,
            "start_date": "2022-01-01T00:00:00+00:00",
            "end_date": "2023-01-01T00:00:00+00:00",
        }
        self.assertDictEqual(actual_json, expected_json)


class HttpApiJsonConfigTest(JsonConfigTest):

    http_api_type = HttpApi
    ext_api_thing = None

    def setUp(self):
        super().setUp()
        self.ext_api_thing = Thing.objects.create(
            name="extAPI_Thing",
            datasource_type="extAPI",
            group=self.group,
            description="Sample description",
            mqtt_uri="mqtt://broker.example.com",
            mqtt_username="mqtt_user",
            mqtt_password="mqtt_password",
            mqtt_hashed_password="mqtt_hashed_password",
            mqtt_topic="sample_topic",
            qaqc_ctx_window="window_config",
            qaqc_tests=[{"test_key": "test_value"}],
        )
        self.ext_api_thing.save()
        self.createHttpApi()
        expected_json = self.get_expected_json_by_thing(self.ext_api_thing)
        self.assertDictEqual(get_thing_properties(self.ext_api_thing), expected_json)

    def createHttpApi(self):
        self.http_api_type.objects.create(sync_enabled=True)


class DwdApiJsonConfigTest(HttpApiJsonConfigTest):

    http_api_type = DwdApi

    def createHttpApi(self):
        DwdApi.objects.create(
            station_id="12345", thing_id=self.ext_api_thing.id, sync_enabled=True
        )


class TtnApiJsonConfigTest(HttpApiJsonConfigTest):

    http_api_type = TtnApi

    def createHttpApi(self):
        TtnApi.objects.create(
            api_key="foo",
            endpoint_uri="bar",
            thing_id=self.ext_api_thing.id,
            sync_enabled=True,
            sync_interval=60,
        )


class NmApiJsonConfigTest(HttpApiJsonConfigTest):

    http_api_type = NmApi

    def createHttpApi(self):
        NmApi.objects.create(
            time_resolution=60,
            station=self.nm_station,
            thing_id=self.ext_api_thing.id,
            sync_enabled=True,
            sync_interval=60,
        )


class BoschIotApiJsonConfigTest(HttpApiJsonConfigTest):

    http_api_type = BoschIotApi

    def createHttpApi(self):
        BoschIotApi.objects.create(
            username="username",
            password="password",
            sensor_id="station_id",
            endpoint="endpoint",
            period=300,
            thing_id=self.ext_api_thing.id,
            sync_enabled=True,
            sync_interval=60,
        )


# do not run tests on abstract base class
del HttpApiJsonConfigTest
