from tsm.models import (
    DwdApi,
    TtnApi,
    NmApi,
)
from tsm.tests.TestHelpers import TestHelpers
from tsm.utils import (
    get_db_by_thing,
    get_storage_by_thing,
    get_connection_string,
    get_sftp_uri,
    get_sftp_username,
    get_sftp_password,
    get_mqtt_username,
    get_mqtt_password,
    get_random_chars,
    create_db_username,
    get_group_name_with_vo_prefix,
    get_group_name,
    get_group_vo,
    create_bucket_name,
    get_current_http_api_by_thing,
    get_connection_string_secure,
)


class TestUtils(TestHelpers.TestCaseWithBaseModelSetup):
    def test_get_db_by_thing(self):
        db = get_db_by_thing(self.thing)
        self.assertEqual(db, self.db)

    def test_get_storage_by_thing(self):
        storage = get_storage_by_thing(self.thing)
        self.assertEqual(storage, self.storage)

    def test_get_current_http_api_by_thing(self):
        self.thing.http_api_type = DwdApi.get_type_name()
        self.thing.save()
        api_by_thing = get_current_http_api_by_thing(self.thing)
        self.assertEqual(api_by_thing, self.dwd_api)

        self.thing.http_api_type = TtnApi.get_type_name()
        self.thing.save()
        api_by_thing = get_current_http_api_by_thing(self.thing)
        self.assertEqual(api_by_thing, self.ttn_api)

        self.thing.http_api_type = NmApi.get_type_name()
        self.thing.save()
        api_by_thing = get_current_http_api_by_thing(self.thing)
        self.assertEqual(api_by_thing, self.nm_api)

    def test_get_connection_string(self):
        connection_str = get_connection_string(self.thing)
        expected_str = f"postgresql://{self.db.username}:{self.db.password}@{self.db.url}/{self.db.name}"
        self.assertEqual(connection_str, expected_str)
        readonly_connection_str = get_connection_string(self.thing, readonly=True)
        expected_readonly_str = f"postgresql://{self.db.ro_username}:{self.db.ro_password}@{self.db.url}/{self.db.name}"
        self.assertEqual(readonly_connection_str, expected_readonly_str)

    def test_get_connection_string_secure(self):
        connection_str = get_connection_string_secure(self.thing)
        expected_str = f"postgresql://{self.db.username}@{self.db.url}/{self.db.name}"
        self.assertEqual(connection_str, expected_str)
        readonly_connection_str = get_connection_string_secure(
            self.thing, readonly=True
        )
        expected_readonly_str = (
            f"postgresql://{self.db.ro_username}@{self.db.url}/{self.db.name}"
        )
        self.assertEqual(readonly_connection_str, expected_readonly_str)

    def test_get_sftp_uri(self):
        sftp_uri = get_sftp_uri(self.thing)
        self.assertEqual(sftp_uri, self.storage.fileserver_uri)

    def test_get_sftp_username(self):
        sftp_username = get_sftp_username(self.thing)
        self.assertEqual(sftp_username, self.storage.access_key)

    def test_get_sftp_password(self):
        sftp_password = get_sftp_password(self.thing)
        self.assertEqual(sftp_password, self.storage.secret_key)

    def test_get_mqtt_username(self):
        mqtt_username = get_mqtt_username(self.thing)
        self.assertEqual(
            mqtt_username, self.mqtt_username
        )  # Since thing.mqtt_username is not set

    def test_get_mqtt_password(self):
        mqtt_password = get_mqtt_password(self.thing)
        self.assertEqual(
            mqtt_password, self.mqtt_password
        )  # Since thing.mqtt_password is not set

    def test_get_random_chars(self):
        random_chars = get_random_chars(10)
        self.assertEqual(len(random_chars), 10)

    def test_create_db_username(self):
        db_username = create_db_username(self.group)
        self.assertTrue(db_username.startswith("ufztimese_mytestgroup"))

    def test_get_group_name_with_vo_prefix(self):
        group_name_vo_prefix = get_group_name_with_vo_prefix(self.group)
        self.assertEqual(group_name_vo_prefix, "UFZ-Timeseries-Management:MyTestGroup")

    def test_get_group_name(self):
        group_name = get_group_name(self.group)
        self.assertEqual(group_name, "MyTestGroup")

    def test_get_group_vo(self):
        group_vo = get_group_vo(self.group)
        self.assertEqual(group_vo, "UFZ-Timeseries-Management")

    def test_create_bucket_name(self):
        bucket_name = create_bucket_name(self.thing)
        self.assertTrue(bucket_name.startswith("ufz-timese-mytestgroup"))
