import unittest.mock
from http import HTTPStatus

from django.test import TestCase, Client

from django.contrib.auth.models import User, Group
from django.forms import ModelForm

from tsm.models import (
    HttpApi,
    Thing,
    Database,
    RawDataStorage,
    ParserType,
    CsvParser,
    DwdApi,
    TtnApi,
    NmApi,
    NmStation,
    QaqcSetting,
    QaqcTest,
    QaqcFunction,
    TimestampColumn,
)

mock_encrypt_pw = unittest.mock.patch(
    "tsm.utils.encrypt_pw", lambda pw: "[ENCRYPTED]" + pw if pw else None
)


class TestHelpers(object):
    """
    Base TestCase class for testing forms
    """

    class FormTest(TestCase):
        form = ModelForm
        path = ""
        header = ""

        def get_valid_form_payloads(self):
            """List of form payloads resulting in positive validation"""
            return []

        def get_invalid_form_payloads(self):
            """List of form payloads resulting in negative validation"""
            return []

        def get_valid_field_payloads(self):
            """List of single field payloads resulting in positive validation"""
            return []

        def get_invalid_field_payloads(self):
            """List of single field payloads resulting in negative validation"""
            return []

        def get_included_fields(self):
            """List of fields that are included in the form"""
            return []

        def get_excluded_fields(self):
            """List of fields that are excluded from the form"""
            return []

        def get_required_fields(self):
            """List of fields that are required in the form"""
            return []

        def get_required_fields_error_messages(self):
            """List of error messages for required fields"""
            return [["This field could not be empty."], ["This field is required."]]

        def setUp(self):
            self.password = "PASSWORD"
            self.user = User.objects.create_superuser(
                username="ADMIN", password=self.password
            )
            self.user.set_password(self.password)
            self.user.save()

        # client helper functions

        def client_login(self):
            self.client = Client()
            self.client.login(username=self.user.username, password=self.password)

        # base assertions

        def assertFormValid(self, form_data, name):
            form = self.form(data=form_data)
            self.assertTrue(
                form.is_valid(),
                msg=f"`{name}` was not valid. Unexpected errors: {form.errors}",
            )

        def assertFormInvalid(self, form_data, name):
            form = self.form(data=form_data)
            self.assertFalse(form.is_valid(), msg=f"`{name}` was not invalid")

        def assertFieldValid(self, field, data, name):
            form = self.form(data={field: data})
            self.assertNotIn(
                field,
                form.errors,
                msg=f"`{name}` was not valid. Unexpected errors: {form.errors[field] if field in form.errors else None}",
            )

        def assertFieldInvalid(self, field, data, expected_error_message, name):
            form = self.form(data={field: data})
            self.assertFormError(
                form,
                field,
                [expected_error_message],
                msg_prefix=f"`{name}` was not invalid or resulted in incorrect errors",
            )

        def assertExcludedFields(self, excluded_fields):
            form = self.form
            fields = list(form().base_fields)

            for field in list(form().declared_fields):
                if field not in fields:
                    fields.append(field)

            for field in excluded_fields:
                self.assertNotIn(field, fields)

        def assertIncludedFields(self, included_fields):
            form = self.form
            fields = list(form().base_fields)

            for field in list(form().declared_fields):
                if field not in fields:
                    fields.append(field)

            for field in included_fields:
                self.assertIn(field, fields)

        def assertRequiredErrors(self, payload, required_fields):
            form = self.form(data=payload)
            for key in required_fields:
                self.assertIn(key, form.errors)
                self.assertIn(
                    form.errors[key], self.get_required_fields_error_messages()
                )

        # base unit tests

        def test_included_fields(self):
            """Verifies if all included properties are actually included fields in the form"""
            self.assertIncludedFields(self.get_included_fields())

        def test_excluded_fields(self):
            """Verifies if all excluded properties are actually excluded from the form"""
            self.assertExcludedFields(self.get_excluded_fields())

        def test_required_fields(self):
            """Verifies if all required properties are actually required fields in the form"""
            self.assertRequiredErrors(
                payload={},
                required_fields=self.get_required_fields(),
            )

        def test_invalid_payloads(self):
            """Verifies negative validation of invalid payloads"""
            for entry in self.get_invalid_form_payloads():
                self.assertFormInvalid(entry["payload"], name=entry["name"])

            for entry in self.get_invalid_field_payloads():
                self.assertFieldInvalid(
                    field=entry["field"],
                    data=entry["data"],
                    expected_error_message=entry["expected_error_message"],
                    name=entry["name"],
                )

        def test_valid_payloads(self):
            """Verifies positive validation of valid payloads"""
            for entry in self.get_valid_form_payloads():
                self.assertFormValid(form_data=entry["payload"], name=entry["name"])

            for entry in self.get_valid_field_payloads():
                self.assertFieldValid(
                    field=entry["field"], data=entry["data"], name=entry["name"]
                )

        # base integration tests

        def test_get(self):
            """Verifies correct integration of the form"""
            if not self.path:
                return

            self.client_login()
            response = self.client.get(self.path)

            self.assertEqual(response.status_code, HTTPStatus.OK)

            if self.header:
                self.assertContains(response, self.header, html=True)

        def test_post_error(self):
            """Verifies correct integration of the form after successful submit"""
            if not self.path:
                return

            self.client_login()
            response = self.client.post(self.path, data={})

            self.assertEqual(response.status_code, HTTPStatus.OK)
            self.assertContains(response, "This field is required.", html=True)

        class Meta:
            abstract = True

    class HttpApiTestCase(TestCase):
        """
        Base TestCase class for testing Http Api subclasses/models
        """

        class Meta:
            abstract = True

        api_type = HttpApi

        def setUp(self):
            self.group = Group.objects.create(name="SampleGroup")
            self.thing = Thing.objects.create(
                name="SampleThing",
                group=self.group,
                http_api_type=self.api_type.get_type_name(),
            )
            self.api = self.api_type.objects.create(thing=self.thing)

        def test_api_creation(self):
            """
            Test that an Api instance can be created
            """
            self.assertEqual(self.api.thing, self.thing)

        def test_api_str_method(self):
            """
            Test the __str__ method of Api model
            """
            expected_str = str(self.api.id)
            self.assertEqual(str(self.api), expected_str)

        def test_api_deletion(self):
            """
            Test that an Api instance can be deleted
            """
            api_id = self.api.id
            self.api.delete()
            self.assertFalse(self.api_type.objects.filter(id=api_id).exists())

        def test_api_update(self):
            """
            Test that an Api instance can be updated
            """
            new_thing = Thing.objects.create(
                name="NewSampleThing",
                group=self.group,
                http_api_type=self.api_type.get_type_name(),
            )
            self.api.thing = new_thing
            self.api.save()
            updated_api = self.api_type.objects.get(id=self.api.id)
            self.assertEqual(updated_api.thing, new_thing)

        def test_api_invalid_creation(self):
            """
            Test that an Api instance cannot be created without required fields
            """
            with self.assertRaises(Exception):
                self.api_type.objects.create()

    class QaqcTestFunctionTestCase(TestCase):
        """
        Base TestCase class for testing Http Api subclasses/models
        """

        class Meta:
            abstract = True

        qaqc_function_type = QaqcFunction

        def setUp(self):
            self.group = Group.objects.create(name="SampleGroup")
            self.qaqc_setting = QaqcSetting.objects.create(
                name="SampleQaqcSetting", context_window="foo", group=self.group
            )
            self.qaqc_test = QaqcTest.objects.create(
                qaqc_setting=self.qaqc_setting, name="SampleQaqcTest"
            )
            self.parent_qaqc_function = QaqcFunction.objects.create(
                qaqc_test=self.qaqc_test
            )
            self.qaqc_function = self.qaqc_function_type.objects.create(
                qaqc_test=self.qaqc_test,
            )

        def test_qaqc_function_creation(self):
            """
            Test that a Qaqc Function instance can be created
            """
            self.assertEqual(self.qaqc_function.qaqc_test, self.qaqc_test)

        def test_qaqc_function_str_method(self):
            """
            Test the __str__ method of Qaqc Function model
            """
            expected_str = str(self.qaqc_function.id)
            self.assertEqual(str(self.qaqc_function), expected_str)

        def test_qaqc_function_update(self):
            """
            Test that a Qaqc Function instance can be updated
            """
            new_qaqc_test = QaqcTest.objects.create(
                name="NewSampleQaqcTest",
                qaqc_setting=self.qaqc_setting,
                qaqc_function_type=self.qaqc_function_type.get_function_name(),
            )
            self.qaqc_function.qaqc_test = new_qaqc_test
            self.qaqc_function.save()
            updated_qaqc_function = self.qaqc_function_type.objects.get(
                id=self.qaqc_function.id
            )
            self.assertEqual(updated_qaqc_function.qaqc_test, new_qaqc_test)

        def test_qaqc_function_invalid_creation(self):
            """
            Test that a Qaqc Function instance cannot be created without required fields
            """
            with self.assertRaises(Exception):
                self.qaqc_function_type.objects.create()

    class TestCaseWithBaseModelSetup(TestCase):
        """
        Base TestCase class for testing utils with several base models and relations
        """

        def setUp(self):
            mock_encrypt_pw.start()

            # Create necessary objects for testing
            self.group = Group.objects.create(
                name="urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:MyTestGroup#login-dev.helmholtz.de"
            )
            self.thing = Thing.objects.create(name="Test Thing", group=self.group)
            self.db = Database.objects.get(group=self.thing.group)
            self.mqtt_username = self.thing.mqtt_username
            self.mqtt_password = self.thing.mqtt_password

            existing_datastorage: RawDataStorage = RawDataStorage.objects.filter(
                thing=self.thing
            ).first()

            existing_datastorage.access_key = "sample_access_key"
            existing_datastorage.secret_key = "sample_secret_key"
            existing_datastorage.fileserver_uri = "sftp://example.com"
            existing_datastorage.bucket = "sample_bucket"

            existing_datastorage.save()  # update the model also in the db

            self.storage = existing_datastorage

            self.parser_type = ParserType.objects.create(name="SampleParserType")

            self.csv_parser = CsvParser.objects.create(
                type=self.parser_type,
                group=self.group,
                name="SampleParser",
                delimiter=",",
                exclude_headlines=1,
                exclude_footlines=1,
                pandas_read_csv={"option": "value"},
            )

            # Create TimestampColumn instances separately
            TimestampColumn.objects.create(
                csv_parser=self.csv_parser, column=1, format="%Y-%m-%d"
            )

            parent_parser_instance = self.csv_parser.parser_ptr
            self.parser = parent_parser_instance
            self.thing.parser.add(self.parser)

            self.thing.http_api_type = DwdApi.get_type_name()
            self.thing.save()
            self.dwd_api = DwdApi.objects.create(
                station_id="12345", thing_id=self.thing.id, sync_enabled=True
            )

            self.thing.http_api_type = TtnApi.get_type_name()
            self.thing.save()
            self.ttn_api = TtnApi.objects.create(
                api_key="foo",
                endpoint_uri="bar",
                thing_id=self.thing.id,
                sync_enabled=True,
                sync_interval=60,
            )

            self.nm_station = NmStation.objects.create(station_id="TEST")
            self.thing.http_api_type = NmApi.get_type_name()
            self.thing.save()
            self.nm_api = NmApi.objects.create(
                time_resolution=60,
                station=self.nm_station,
                thing_id=self.thing.id,
                sync_enabled=True,
                sync_interval=60,
            )

            self.qaqc_setting = QaqcSetting.objects.create(
                name="SampleQAQCSetting", context_window="foo", group=self.group
            )

            # Enable full logging of dissimilar values
            self.maxDiff = None

        def tearDown(self):
            mock_encrypt_pw.stop()
