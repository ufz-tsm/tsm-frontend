from tsm.models import (
    Thing,
    DwdApi,
    TtnApi,
    NmApi,
    BoschIotApi,
    NmStation,
)
from django.contrib.auth.models import Group
from tsm.form_classes import NmStationForm
from tsm.form_classes import DwdApiForm
from tsm.form_classes import TtnApiForm
from tsm.form_classes import NmApiForm
from tsm.form_classes import BoschIotApiForm
from tsm.form_classes import UbaAirDataApiForm
from tsm.form_classes import TSystemsApiForm

from tsm.tests.TestHelpers import TestHelpers


class DwdApiFormTest(TestHelpers.FormTest):
    form = DwdApiForm

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.thing = Thing.objects.create(
            name="SampleGroup",
            group=self.group,
            http_api_type=DwdApi.get_type_name(),
            datasource_type="extAPI",
        )

        super().setUp()

    def get_required_fields(self):
        return ["station_id", "thing"]

    def get_included_fields(self):
        return ["station_id", "thing"]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "Valid DWD API settings",
                "payload": {
                    "thing": self.thing.id,
                    "station_id": "12345",
                },
            },
        ]

    def get_invalid_field_payloads(self):
        return [
            {
                "name": "DWD API - station_id - ID too long",
                "field": "station_id",
                "data": "123456",
                "expected_error_message": "Ensure this value has exactly 5 characters.",
            },
        ]


class TtnApiFormTest(TestHelpers.FormTest):
    form = TtnApiForm

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.thing = Thing.objects.create(
            name="SampleGroup",
            group=self.group,
            http_api_type=TtnApi.get_type_name(),
            datasource_type="extAPI",
        )

        super().setUp()

    def get_required_fields(self):
        return ["thing", "endpoint_uri", "api_key"]

    def get_included_fields(self):
        return ["thing", "endpoint_uri", "api_key"]


class NmApiFormTest(TestHelpers.FormTest):
    form = NmApiForm

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.thing = Thing.objects.create(
            name="SampleGroup",
            group=self.group,
            http_api_type=NmApi.get_type_name(),
            datasource_type="extAPI",
        )
        self.nm_station = NmStation.objects.create(station_id="TEST")

        super().setUp()

    def get_required_fields(self):
        return ["thing", "station", "time_resolution"]

    def get_included_fields(self):
        return ["thing", "station", "time_resolution"]


class BoschIotApiFormTest(TestHelpers.FormTest):
    form = BoschIotApiForm

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.thing = Thing.objects.create(
            name="SampleGroup",
            group=self.group,
            http_api_type=BoschIotApi.get_type_name(),
            datasource_type="extAPI",
        )

        super().setUp()

    def get_required_fields(self):
        return ["thing", "sensor_id", "username", "password", "period", "endpoint"]

    def get_included_fields(self):
        return ["thing", "sensor_id", "username", "password", "period", "endpoint"]


class NmStationFormTest(TestHelpers.FormTest):
    form = NmStationForm

    def get_required_fields(self):
        return ["station_id"]

    def get_included_fields(self):
        return ["station_id", "description"]


class UbaAirDataApiFormTest(TestHelpers.FormTest):
    form = UbaAirDataApiForm

    def get_required_fields(self):
        return ["station_id"]

    def get_included_fields(self):
        return ["station_id"]


class TSystemsApiFormTest(TestHelpers.FormTest):
    form = TSystemsApiForm

    def get_required_fields(self):
        return ["station_id", "group", "username", "password"]

    def get_included_fields(self):
        return ["station_id", "group", "username", "password"]
