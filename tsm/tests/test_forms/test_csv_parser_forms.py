from django.contrib.auth.models import Group
from tsm.form_classes import CsvParserFormAdmin
from tsm.form_classes import CsvParserFormUser

from tsm.tests.TestHelpers import TestHelpers


class CsvParserFormAdminTest(TestHelpers.FormTest):
    form = CsvParserFormAdmin
    path = "/tsm/csvparser/add/"
    header = "<h1>Add csv parser</h1>"

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")

        super().setUp()

    def get_included_fields(self):
        return [
            "name",
            "group",
            "delimiter",
            "exclude_headlines",
            "exclude_footlines",
            "pandas_read_csv",
        ]

    def get_required_fields(self):
        return ["name", "group"]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "CSV parser",
                "payload": {
                    "name": "SampleParser",
                    "group": self.group,
                    "delimiter": ",",
                    "exclude_headlines": "1",
                    "exclude_footlines": "1",
                    "timestamp_columns": '[{"column":1, "format": "%Y-%m-%d"}]',
                    "pandas_read_csv": "null",
                },
            },
        ]

    def get_valid_field_payloads(self):
        return [
            {
                "name": "pandas_read_csv - JSON object",
                "field": "pandas_read_csv",
                "data": "{}",
            },
            {
                "name": "pandas_read_csv - null",
                "field": "pandas_read_csv",
                "data": "null",
            },
            {"name": "pandas_read_csv - empty", "field": "pandas_read_csv", "data": ""},
        ]

    def get_invalid_field_payloads(self):
        return [
            {
                "name": "pandas_read_csv - invalid JSON",
                "field": "pandas_read_csv",
                "data": "invalid json",
                "expected_error_message": "Enter a valid JSON.",
            },
            {
                "name": "pandas_read_csv - JSON array",
                "field": "pandas_read_csv",
                "data": "[]",
                "expected_error_message": "Expected a JSON object (curly braces) or 'null'.",
            },
        ]


class CsvParserFormUserTest(TestHelpers.FormTest):
    form = CsvParserFormUser

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")

        super().setUp()

    def get_valid_form_payloads(self):
        return [
            {
                "name": "CSV parser",
                "payload": {
                    "name": "SampleParser",
                    "group": self.group,
                    "delimiter": ",",
                    "exclude_headlines": "1",
                    "exclude_footlines": "1",
                    "timestamp_columns": '[{"column":1, "format": "%Y-%m-%d"}]',
                },
            },
        ]
