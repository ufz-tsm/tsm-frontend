from django.contrib.auth.models import Group

from tsm.form_classes import (
    QaqcSettingRunForm,
)
from tsm.models import QaqcSetting

from tsm.tests.TestHelpers import TestHelpers


class QaqcRunFormTest(TestHelpers.FormTest):
    form = QaqcSettingRunForm
    path = "/tsm/qaqcsetting/run-qaqc-form/?ids=1"
    header = "<h2>Run QA/QC Setting</h2>"

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.qaqc_setting = QaqcSetting.objects.create(
            name="SampleQAQCSetting", context_window="foo", group=self.group
        )

        super().setUp()

    def test_required_fields(self):
        self.assertRequiredErrors(
            {},
            ["begin_date", "end_date"],
        )

    def get_included_fields(self):
        return ["begin_date", "end_date"]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "Valid date range",
                "payload": {
                    "begin_date": "2022-01-01 00:00:00",
                    "end_date": "2022-12-31 00:00:00",
                },
            },
        ]

    def get_invalid_form_payloads(self):
        return [
            {
                "name": "Begin date equals end date",
                "payload": {
                    "begin_date": "2022-01-01 00:00:00",
                    "end_date": "2022-01-01 00:00:00",
                },
            },
            {
                "name": "Begin date after end date",
                "payload": {
                    "begin_date": "2023-01-01 00:00:00",
                    "end_date": "2022-01-01 00:00:00",
                },
            },
        ]

    def get_invalid_field_payloads(self):
        return [
            {
                "name": "Invalid begin date format",
                "field": "begin_date",
                "data": "invalid date format",
                "expected_error_message": "Enter a valid date/time.",
            },
            {
                "name": "Invalid end date format",
                "field": "end_date",
                "data": "invalid date format",
                "expected_error_message": "Enter a valid date/time.",
            },
        ]
