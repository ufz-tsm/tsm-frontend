from django.contrib.auth.models import Group

from tsm.form_classes import (
    ThingForm,
    ThingFormEditParserWasNotSelected,
    ThingFormEditParserWasSelected,
)
from tsm.models import MqttDeviceType, ParserType, Parser

from tsm.tests.TestHelpers import TestHelpers


class ThingFormTest(TestHelpers.FormTest):
    form = ThingForm
    path = "/tsm/thing/add/"
    header = "<h1>Add Thing</h1>"

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.mqtt_device_type = MqttDeviceType.objects.create(
            name="SampleMqttDeviceType"
        )
        self.parser_type = ParserType.objects.create(name="SampleParserType")
        self.parser = Parser.objects.create(name="SampleParser", group=self.group)

        super().setUp()

    def test_required_fields(self):
        self.assertRequiredErrors(
            {"datasource_type": "MQTT"},
            ["name", "group", "mqtt_device_type", "mqtt_uri", "mqtt_topic"],
        )
        self.assertRequiredErrors(
            {"datasource_type": "SFTP"},
            [
                "name",
                "group",
                "parser_type",
                "sftp_filename_pattern",
                "parser",
            ],
        )
        self.assertRequiredErrors(
            {"datasource_type": "extSFTP"},
            [
                "name",
                "group",
                "parser_type",
                "sftp_filename_pattern",
                "parser",
                "ext_sftp_path",
                "ext_sftp_sync_interval",
                "ext_sftp_uri",
                "ext_sftp_username",
            ],
        )

    def get_included_fields(self):
        return [
            "name",
            "group",
            "parser_type",
            "description",
            "sftp_filename_pattern",
            "mqtt_uri",
            "mqtt_username",
            "mqtt_password",
            "mqtt_topic",
            "mqtt_device_type",
            "qaqc_ctx_window",
            "qaqc_tests",
            "datasource_type",
            "parser",
            "ext_sftp_password",
            "ext_sftp_sync_enabled",
            "ext_sftp_path",
            "ext_sftp_public_key",
            "ext_sftp_sync_interval",
            "ext_sftp_uri",
            "ext_sftp_username",
        ]

    def get_excluded_fields(self):
        return ["thing_id", "is_created"]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "SFTP Thing with SFTP properties",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "SFTP",
                    "parser": self.parser.id,
                },
            },
            {
                "name": "SFTP Thing with all properties",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "mqtt_uri": "mqtt://broker.example.com",
                    "mqtt_username": "mqtt_user",
                    "mqtt_password": "mqtt_password",
                    "mqtt_topic": "sample_topic",
                    "mqtt_device_type": self.mqtt_device_type,
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "SFTP",
                    "parser": self.parser.id,
                },
            },
            {
                "name": "MQTT Thing with MQTT properties",
                "payload": {
                    "name": "AnotherSampleThing",
                    "group": self.group,
                    "description": "Sample description",
                    "mqtt_uri": "mqtt://broker.example.com",
                    "mqtt_username": "mqtt_user",
                    "mqtt_password": "mqtt_password",
                    "mqtt_topic": "sample_topic",
                    "mqtt_device_type": self.mqtt_device_type,
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "MQTT",
                },
            },
            {
                "name": "MQTT Thing with all properties",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "mqtt_uri": "mqtt://broker.example.com",
                    "mqtt_username": "mqtt_user",
                    "mqtt_password": "mqtt_password",
                    "mqtt_topic": "sample_topic",
                    "mqtt_device_type": self.mqtt_device_type,
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "MQTT",
                    "parser": self.parser.id,
                },
            },
            {
                "name": "extSFTP Thing with extSFTP properties",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "extSFTP",
                    "parser": self.parser.id,
                    "ext_sftp_sync_enabled": True,
                    "ext_sftp_password": "password",
                    "ext_sftp_path": "/some/path",
                    "ext_sftp_sync_interval_0": "1",
                    "ext_sftp_sync_interval_1": "d",
                    "ext_sftp_uri": "some.uri",
                    "ext_sftp_username": "username",
                },
            },
        ]

    def get_invalid_form_payloads(self):
        return [
            {
                "name": "group missing",
                "payload": {
                    "name": "SampleThing",
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "SFTP",
                    "parser": self.parser.id,
                },
            },
            {
                "name": "sync interval below minimum",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "extSFTP",
                    "parser": self.parser.id,
                    "ext_sftp_sync_enabled": True,
                    "ext_sftp_password": "password",
                    "ext_sftp_path": "/some/path",
                    "ext_sftp_sync_interval_0": "9",
                    "ext_sftp_sync_interval_1": "min",
                    "ext_sftp_uri": "some.uri",
                    "ext_sftp_username": "username",
                },
            },
        ]

    def get_invalid_field_payloads(self):
        return [
            {
                "name": "qaqc_tests - invalid JSON",
                "field": "qaqc_tests",
                "data": "invalid JSON",
                "expected_error_message": "Enter a valid JSON.",
            },
            {
                "name": "qaqc_tests - invalid JSON",
                "field": "qaqc_tests",
                "data": '{"no": "array"}',
                "expected_error_message": "Expected a JSON array.",
            },
        ]


class ThingFormEditParserWasSelectedTest(TestHelpers.FormTest):
    form = ThingFormEditParserWasSelected

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.mqtt_device_type = MqttDeviceType.objects.create(
            name="SampleMqttDeviceType"
        )
        self.parser_type = ParserType.objects.create(name="SampleParserType")
        self.parser = Parser.objects.create(name="SampleParser", group=self.group)

        super().setUp()

    def get_included_fields(self):
        return [
            "name",
            "group",
            "parser_type",
            "description",
            "sftp_filename_pattern",
            "mqtt_uri",
            "mqtt_username",
            "mqtt_password",
            "mqtt_topic",
            "mqtt_device_type",
            "qaqc_ctx_window",
            "qaqc_tests",
            "datasource_type",
            "parser",
        ]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "SFTP Thing with SFTP properties",
                "payload": {
                    "name": "SampleThing",
                    "group": self.group,
                    "group_text": "SampleGroupText",
                    "parser_type": self.parser_type.id,
                    "description": "Sample description",
                    "sftp_filename_pattern": "*.txt",
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "SFTP",
                    "parser": self.parser.id,
                },
            }
        ]


class ThingFormEditParserWasNotSelectedTest(TestHelpers.FormTest):
    form = ThingFormEditParserWasNotSelected

    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.mqtt_device_type = MqttDeviceType.objects.create(
            name="SampleMqttDeviceType"
        )
        self.parser_type = ParserType.objects.create(name="SampleParserType")
        self.parser = Parser.objects.create(name="SampleParser", group=self.group)

        super().setUp()

    def get_included_fields(self):
        return [
            "name",
            "group",
            "parser_type",
            "description",
            "sftp_filename_pattern",
            "mqtt_uri",
            "mqtt_username",
            "mqtt_password",
            "mqtt_topic",
            "mqtt_device_type",
            "qaqc_ctx_window",
            "qaqc_tests",
            "datasource_type",
            "parser",
        ]

    def get_valid_form_payloads(self):
        return [
            {
                "name": "MQTT Thing with MQTT properties",
                "payload": {
                    "name": "AnotherSampleThing",
                    "group": self.group,
                    "group_text": "SampleGroupText",
                    "description": "Sample description",
                    "mqtt_uri": "mqtt://broker.example.com",
                    "mqtt_username": "mqtt_user",
                    "mqtt_password": "mqtt_password",
                    "mqtt_topic": "sample_topic",
                    "mqtt_device_type": self.mqtt_device_type,
                    "qaqc_ctx_window": "window_config",
                    "qaqc_tests": [{"test_key": "test_value"}],
                    "datasource_type": "MQTT",
                },
            },
        ]
