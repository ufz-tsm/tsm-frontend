from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import QaqcSetting, QaqcTest


class QaqcSettingTest(TestCase):
    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.qaqc_setting = QaqcSetting.objects.create(
            name="SampleQAQCSetting", context_window="foo", group=self.group
        )

    def test_str_representation(self):
        expected_str = self.qaqc_setting.name
        self.assertEqual(str(self.qaqc_setting), expected_str)

    def test_group_foreign_key(self):
        self.assertEqual(self.qaqc_setting.group, self.group)

    def test_required_fields(self):
        with self.assertRaises(Exception):
            QaqcSetting.objects.create(name="SampleQAQCSetting", context_window="foo")

        with self.assertRaises(Exception):
            QaqcSetting.objects.create(context_window="foo", group=self.group)

        with self.assertRaises(Exception):
            QaqcSetting.objects.create(name="SampleQAQCSetting", group=self.group)

    def test_one_to_many_relationship(self):
        for i in range(3):
            QaqcTest.objects.create(qaqc_setting=self.qaqc_setting, name=str(i))
        self.assertEqual(
            QaqcTest.objects.filter(qaqc_setting=self.qaqc_setting).count(), 3
        )

    def test_only_one_setting_per_project(self):
        QaqcSetting.objects.create(
            name="OtherQAQCSetting", context_window="foo", group=self.group, active=True
        )

        self.qaqc_setting.active = True
        self.qaqc_setting.save()

        self.assertEqual(QaqcSetting.objects.filter(active=True).count(), 1)
        self.assertEqual(QaqcSetting.objects.get(name="SampleQAQCSetting").active, True)
        self.assertEqual(QaqcSetting.objects.get(name="OtherQAQCSetting").active, False)
