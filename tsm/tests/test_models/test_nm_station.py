from django.test import TestCase

from tsm.models import NmStation


class NmStationModelTest(TestCase):
    def setUp(self):
        self.nm_station = NmStation.objects.create(
            station_id="TEST", description="SampleDescription"
        )

    def test_str_representation(self):
        expected_str = "TEST"
        self.assertEqual(str(self.nm_station), expected_str)

    def test_model_create(self):
        nm_station_count_before = NmStation.objects.count()
        new_nm_station = NmStation.objects.create(
            station_id="TEST2", description="SampleDescription"
        )
        nm_station_count_after = NmStation.objects.count()

        self.assertEqual(nm_station_count_after, nm_station_count_before + 1)
        self.assertEqual(new_nm_station, NmStation.objects.get(station_id="TEST2"))

    def test_model_save_uppercase_transformation(self):
        self.nm_station = NmStation.objects.create(
            station_id="lower", description="SampleDescription"
        )
        self.nm_station.save()
        self.assertEqual(NmStation.objects.filter(station_id="LOWER").count(), 1)

    def test_model_delete(self):
        nm_station_count_before = NmStation.objects.count()
        self.nm_station.delete()
        nm_station_count_after = NmStation.objects.count()

        self.assertEqual(nm_station_count_after, nm_station_count_before - 1)
