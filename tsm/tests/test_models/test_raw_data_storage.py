from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import Thing, RawDataStorage


class RawDataStorageModelTest(TestCase):
    def setUp(self):
        # Create a sample Thing instance for testing
        self.group = Group.objects.create(name="SampleGroup")
        self.thing = Thing.objects.create(name="SampleThing", group=self.group)

        # Search for existing instance as it gets created when a new thing is saved
        existing_datastorage: RawDataStorage = RawDataStorage.objects.filter(
            thing=self.thing
        ).first()

        existing_datastorage.access_key = "sample_access_key"
        existing_datastorage.secret_key = "sample_secret_key"
        existing_datastorage.fileserver_uri = "http://fileserver.example.com"
        existing_datastorage.bucket = "sample_bucket"

        self.raw_data_storage = existing_datastorage

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "sample_bucket"
        self.assertEqual(str(self.raw_data_storage), expected_str)

    def test_bucket_max_length(self):
        # Test that the max_length attribute of the bucket field is set correctly
        max_length = self.raw_data_storage._meta.get_field("bucket").max_length
        self.assertEqual(max_length, 63)

    def test_access_key_max_length(self):
        # Test that the max_length attribute of the access_key field is set correctly
        max_length = self.raw_data_storage._meta.get_field("access_key").max_length
        self.assertEqual(max_length, 200)

    def test_secret_key_max_length(self):
        # Test that the max_length attribute of the secret_key field is set correctly
        max_length = self.raw_data_storage._meta.get_field("secret_key").max_length
        self.assertEqual(max_length, 200)

    def test_fileserver_uri_blank_and_null(self):
        # Test that the fileserver_uri field allows blank and null values
        self.assertEqual(
            self.raw_data_storage.fileserver_uri, "http://fileserver.example.com"
        )

    def test_thing_one_to_one_relationship(self):
        # Test that the OneToOne relationship with Thing is set up correctly
        self.assertEqual(self.raw_data_storage.thing, self.thing)
