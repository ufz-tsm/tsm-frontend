from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import Database


class DatabaseModelTest(TestCase):
    def setUp(self):
        # Create a sample Group instance for testing
        self.group = Group.objects.create(name="SampleGroup")

        # Create a sample Database instance for testing
        self.database = Database.objects.create(
            url="http://example.com/db",
            name="SampleDB",
            username="admin",
            ro_username="readonly_user",
            password="admin_password",
            ro_password="readonly_password",
            group=self.group,
        )

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "admin"
        self.assertEqual(str(self.database), expected_str)

    def test_url_max_length(self):
        # Test that the max_length attribute of the url field is set correctly
        max_length = self.database._meta.get_field("url").max_length
        self.assertEqual(max_length, 1000)

    def test_name_max_length(self):
        # Test that the max_length attribute of the name field is set correctly
        max_length = self.database._meta.get_field("name").max_length
        self.assertEqual(max_length, 1000)

    def test_username_max_length(self):
        # Test that the max_length attribute of the username field is set correctly
        max_length = self.database._meta.get_field("username").max_length
        self.assertEqual(max_length, 200)

    def test_ro_username_max_length(self):
        # Test that the max_length attribute of the ro_username field is set correctly
        max_length = self.database._meta.get_field("ro_username").max_length
        self.assertEqual(max_length, 200)

    def test_password_max_length(self):
        # Test that the max_length attribute of the password field is set correctly
        max_length = self.database._meta.get_field("password").max_length
        self.assertEqual(max_length, 200)

    def test_ro_password_max_length(self):
        # Test that the max_length attribute of the ro_password field is set correctly
        max_length = self.database._meta.get_field("ro_password").max_length
        self.assertEqual(max_length, 200)

    def test_group_foreign_key(self):
        # Test that the foreign key relationship with Group is set up correctly
        self.assertEqual(self.database.group, self.group)
