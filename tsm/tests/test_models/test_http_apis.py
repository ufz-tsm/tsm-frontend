from tsm.models import DwdApi, NmApi, TtnApi, BoschIotApi, UbaAirDataApi, TSystemsApi
from tsm.tests.TestHelpers import TestHelpers


class DwdApiTest(TestHelpers.HttpApiTestCase):
    api_type = DwdApi


class NmApiTest(TestHelpers.HttpApiTestCase):
    api_type = NmApi


class TtnApiTest(TestHelpers.HttpApiTestCase):
    api_type = TtnApi


class BoschIotApiTest(TestHelpers.HttpApiTestCase):
    api_type = BoschIotApi


class UbaAirDataApiTest(TestHelpers.HttpApiTestCase):
    api_type = UbaAirDataApi


class TSystemsApiTest(TestHelpers.HttpApiTestCase):
    api_type = TSystemsApi
