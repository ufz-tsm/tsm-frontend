from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import QaqcTest, QaqcSetting


class QaqcTestTest(TestCase):
    def setUp(self):
        self.group = Group.objects.create(name="SampleGroup")
        self.qaqc_setting = QaqcSetting.objects.create(
            name="SampleQaQCSetting", context_window="foo", group=self.group
        )
        self.qaqc_test = QaqcTest.objects.create(
            qaqc_setting=self.qaqc_setting,
            name="SampleQaQCTest",
            field="",
            target="",
            qaqc_function_type="freetext",
        )

    def test_str_representation(self):
        expected_str = self.qaqc_test.name
        self.assertEqual(str(self.qaqc_test), expected_str)

    def test_group_foreign_key(self):
        self.assertEqual(self.qaqc_test.qaqc_setting, self.qaqc_setting)

    def test_required_fields(self):
        with self.assertRaises(Exception):
            QaqcTest.objects.create(field="", target="", qaqc_function_type="freetext")
        with self.assertRaises(Exception):
            QaqcTest.objects.create(
                name="SampleQaQCTest", target="", qaqc_function_type="freetext"
            )
        with self.assertRaises(Exception):
            QaqcTest.objects.create(
                name="SampleQaQCTest", field="", qaqc_function_type="freetext"
            )
        with self.assertRaises(Exception):
            QaqcTest.objects.create(name="SampleQaQCTest", field="", target="")
