from django.test import TestCase

from tsm.models import MqttDeviceType


class MqttDeviceTypeModelTest(TestCase):
    def setUp(self):
        # Create a sample MqttDeviceType instance for testing
        self.device_type = MqttDeviceType.objects.create(name="TestDeviceType")

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "TestDeviceType"
        self.assertEqual(str(self.device_type), expected_str)

    def test_name_max_length(self):
        # Test that the max_length attribute of the name field is set correctly
        max_length = self.device_type._meta.get_field("name").max_length
        self.assertEqual(max_length, 200)

    def test_model_save(self):
        # Test that the model can be saved to the database
        device_type_count_before = MqttDeviceType.objects.count()

        # Create a new MqttDeviceType instance and save it
        new_device_type = MqttDeviceType(name="NewDeviceType")
        new_device_type.save()

        device_type_count_after = MqttDeviceType.objects.count()

        # Check that the count of MqttDeviceType instances increased by 1
        self.assertEqual(device_type_count_after, device_type_count_before + 1)

    def test_model_delete(self):
        # Test that the model can be deleted from the database
        device_type_count_before = MqttDeviceType.objects.count()

        # Delete the existing MqttDeviceType instance
        self.device_type.delete()

        device_type_count_after = MqttDeviceType.objects.count()

        # Check that the count of MqttDeviceType instances decreased by 1
        self.assertEqual(device_type_count_after, device_type_count_before - 1)
