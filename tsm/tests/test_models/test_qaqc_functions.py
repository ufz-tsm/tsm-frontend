from tsm.models import FreeTextFunction
from tsm.tests.TestHelpers import TestHelpers


class FreeTextFunctionTest(TestHelpers.QaqcTestFunctionTestCase):
    qaqc_function_type = FreeTextFunction
