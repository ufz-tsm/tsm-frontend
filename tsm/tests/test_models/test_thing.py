import uuid

from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import MqttDeviceType, Parser, Thing


class ThingModelTest(TestCase):
    def setUp(self):
        # Create sample Group, MqttDeviceType, and Parser instances for testing
        self.group = Group.objects.create(name="SampleGroup")
        self.mqtt_device_type = MqttDeviceType.objects.create(
            name="SampleMqttDeviceType"
        )
        self.parser = Parser.objects.create(name="SampleParser", group=self.group)

        # Create a sample Thing instance for testing
        self.thing = Thing.objects.create(
            name="SampleThing",
            group=self.group,
            description="Sample description",
            sftp_filename_pattern="*.txt",
            mqtt_uri="mqtt://broker.example.com",
            mqtt_username="mqtt_user",
            mqtt_password="mqtt_password",
            mqtt_topic="sample_topic",
            mqtt_device_type=self.mqtt_device_type,
            qaqc_ctx_window="window_config",
            qaqc_tests=[{"test_key": "test_value"}],
        )
        self.thing.parser.add(self.parser)

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "SampleThing"
        self.assertEqual(str(self.thing), expected_str)

    def test_thing_id_default_value(self):
        # Test that the default value for thing_id is a valid UUID
        self.assertTrue(uuid.UUID(str(self.thing.thing_id), version=4))

    def test_datasource_type_choices(self):
        # Test that datasource_type only allows specified choices
        allowed_choices = {"SFTP", "MQTT"}
        self.assertIn(self.thing.datasource_type, allowed_choices)

    def test_group_foreign_key(self):
        # Test that the foreign key relationship with Group is set up correctly
        self.assertEqual(self.thing.group, self.group)

    def test_mqtt_device_type_foreign_key(self):
        # Test that the foreign key relationship with MqttDeviceType is set up correctly
        self.assertEqual(self.thing.mqtt_device_type, self.mqtt_device_type)

    def test_qaqc_tests_json_field(self):
        # Test that the qaqc_tests field can store JSON data
        expected_json = [{"test_key": "test_value"}]
        self.assertEqual(self.thing.qaqc_tests, expected_json)

    def test_parser_many_to_many_relationship(self):
        # Test that the many-to-many relationship with Parser is set up correctly
        self.assertIn(self.parser, self.thing.parser.all())
