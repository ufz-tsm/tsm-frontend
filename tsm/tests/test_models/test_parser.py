from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import ParserType, Parser


class ParserModelTest(TestCase):
    def setUp(self):
        # Create sample ParserType and Group instances for testing
        self.parser_type = ParserType.objects.create(name="SampleParserType")
        self.group = Group.objects.create(name="SampleGroup")

        # Create a sample Parser instance for testing
        self.parser = Parser.objects.create(
            type=self.parser_type, group=self.group, name="SampleParser"
        )

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "SampleParser"
        self.assertEqual(str(self.parser), expected_str)

    def test_type_foreign_key(self):
        # Test that the foreign key relationship with ParserType is set up correctly
        self.assertEqual(self.parser.type, self.parser_type)

    def test_group_foreign_key(self):
        # Test that the foreign key relationship with Group is set up correctly
        self.assertEqual(self.parser.group, self.group)

    def test_name_max_length(self):
        # Test that the max_length attribute of the name field is set correctly
        max_length = self.parser._meta.get_field("name").max_length
        self.assertEqual(max_length, 1000)

    def test_get_properties_json(self):
        # Test that the JSON properties are generated correctly
        self.assertDictEqual(self.parser.get_properties_json(), {})
