from django.contrib.auth.models import Group
from django.test import TestCase

from tsm.models import ParserType, CsvParser, TimestampColumn


class CsvParserModelTest(TestCase):
    def setUp(self):
        # Create sample ParserType and Group instances for testing
        self.parser_type = ParserType.objects.create(name="SampleParserType")
        self.group = Group.objects.create(name="SampleGroup")

        # Create a sample CsvParser instance for testing
        self.csv_parser = CsvParser.objects.create(
            type=self.parser_type,
            group=self.group,
            name="SampleCsvParser",
            delimiter=",",
            exclude_headlines=1,
            exclude_footlines=1,
            pandas_read_csv={"option": "value"},
        )

        # Create TimestampColumn instances separately
        TimestampColumn.objects.create(
            csv_parser=self.csv_parser, column=1, format="%Y-%m-%d"
        )

    def test_str_representation(self):
        # Test that the __str__ method returns the expected string representation
        expected_str = "SampleCsvParser"
        self.assertEqual(str(self.csv_parser), expected_str)

    def test_delimiter_max_length(self):
        # Test that the max_length attribute of the delimiter field is set correctly
        max_length = self.csv_parser._meta.get_field("delimiter").max_length
        self.assertEqual(max_length, 60)

    def test_exclude_headlines_default_value(self):
        # Test that the default value for exclude_headlines is set correctly
        self.assertEqual(self.csv_parser.exclude_headlines, 1)

    def test_exclude_footlines_default_value(self):
        # Test that the default value for exclude_footlines is set correctly
        self.assertEqual(self.csv_parser.exclude_footlines, 1)

    def test_timestamp_columns_blank_and_null(self):
        # Ensure timestamp columns exist initially
        self.assertEqual(self.csv_parser.timestamp_columns.count(), 1)

        # Remove all timestamp columns and check count
        TimestampColumn.objects.filter(csv_parser=self.csv_parser).delete()
        self.assertEqual(self.csv_parser.timestamp_columns.count(), 0)

    def test_timestamp_columns_format(self):
        # Add an additional timestamp column
        TimestampColumn.objects.create(
            csv_parser=self.csv_parser, column=2, format="%M:%S"
        )

        # Fetch related timestamp columns
        timestamp_columns = list(self.csv_parser.timestamp_columns.all())

        # Assert the number of entries
        self.assertEqual(len(timestamp_columns), 2)

        # Check values of the first timestamp column (from setUp)
        self.assertEqual(timestamp_columns[0].column, 1)
        self.assertEqual(timestamp_columns[0].format, "%Y-%m-%d")

        # Check values of the newly added timestamp column
        self.assertEqual(timestamp_columns[1].column, 2)
        self.assertEqual(timestamp_columns[1].format, "%M:%S")

    def test_timestamp_format_max_length(self):
        # Create timestamp columns with max length format strings
        TimestampColumn.objects.create(
            csv_parser=self.csv_parser, column=2, format="X" * 200
        )
        TimestampColumn.objects.create(
            csv_parser=self.csv_parser, column=3, format="Y" * 200
        )

        # Fetch related timestamp columns
        timestamp_columns = self.csv_parser.timestamp_columns.all()

        for entry in timestamp_columns:
            self.assertLessEqual(len(entry.format), 200)

    def test_get_properties_json(self):
        # Add another timestamp column
        TimestampColumn.objects.create(
            csv_parser=self.csv_parser, column=2, format="%H:%M:%S"
        )

        parser_properties = self.csv_parser.get_properties_json()

        # Expected dictionary
        expected_properties = {
            "delimiter": self.csv_parser.delimiter,
            "skipfooter": self.csv_parser.exclude_footlines,
            "skiprows": self.csv_parser.exclude_headlines,
            "pandas_read_csv": self.csv_parser.pandas_read_csv,
            "timestamp_columns": [
                {"column": 1, "format": "%Y-%m-%d"},  # From setUp
                {"column": 2, "format": "%H:%M:%S"},  # Newly added
            ],
        }

        self.assertDictEqual(parser_properties, expected_properties)
