from __future__ import annotations

import json
import re
import secrets
import paramiko
import string

from .models import (
    Database,
    Parser,
    RawDataStorage,
    Thing,
    CsvParser,
    HttpApi,
    QaqcSetting,
    QaqcTest,
)
from django.contrib.auth.models import Group
import uuid
import os
from cryptography.fernet import Fernet

from tsm.models.qaqc_functions.QaqcFunction import QaqcFunction
from tsm.mqtt_actions import (
    publish_qaqc_setting_data_parsed_config,
    publish_tsystems_historic_data_config,
)


def get_db_by_thing(thing: Thing):
    try:
        return Database.objects.get(group=thing.group)
    except Database.DoesNotExist:
        return None


def get_storage_by_thing(thing: Thing):
    try:
        return RawDataStorage.objects.get(thing_id=thing.id)
    except RawDataStorage.DoesNotExist:
        return None


def get_current_qaqc_function_by_qaqc_test(qaqc_test: QaqcTest):
    if not qaqc_test.qaqc_function_type:
        return None

    qaqc_function = [
        x
        for x in QaqcFunction.__subclasses__()
        if x.get_function_name() == qaqc_test.qaqc_function_type
    ]

    if len(qaqc_function) != 1:
        return None

    qaqc_function = qaqc_function[0]

    try:
        return qaqc_function.objects.get(qaqc_test=qaqc_test)
    except qaqc_function.DoesNotExist:
        return None


def get_current_http_api_by_thing(thing: Thing):
    if not thing.http_api_type:
        return None

    api = [
        x for x in HttpApi.__subclasses__() if x.get_type_name() == thing.http_api_type
    ]

    if len(api) != 1:
        return None

    api = api[0]

    try:
        return api.objects.get(thing_id=thing.id)
    except api.DoesNotExist:
        return None


def get_connection_string(thing: Thing, readonly: bool = False):
    """Return full-fledged database DSN.

    For internal use only.
    Use get_connection_string_secure() for same dsn without password.
    """
    db = get_db_by_thing(thing)
    if db:
        usr = db.ro_username if readonly else db.username
        pwd = db.ro_password if readonly else db.password
        return f"postgresql://{usr}:{pwd}@{db.url}/{db.name}"
    return "-"


def get_connection_string_secure(thing: Thing, readonly: bool = False):
    """Return database DSN without the password."""
    db = get_db_by_thing(thing)
    if db:
        usr = db.ro_username if readonly else db.username
        return f"postgresql://{usr}@{db.url}/{db.name}"
    return "-"


def get_sftp_uri(thing: Thing):
    rawdatastorage = get_storage_by_thing(thing)
    if rawdatastorage:
        return rawdatastorage.fileserver_uri
    else:
        return "-"


def get_sftp_username(thing: Thing):
    rawdatastorage = get_storage_by_thing(thing)
    if rawdatastorage:
        return rawdatastorage.access_key
    else:
        return "-"


def get_sftp_password(thing: Thing):
    rawdatastorage = get_storage_by_thing(thing)
    if rawdatastorage:
        return rawdatastorage.secret_key
    else:
        return "-"


def get_mqtt_username(thing: Thing):
    if thing.mqtt_username:
        return thing.mqtt_username
    else:
        return "-"


def get_mqtt_password(thing: Thing):
    if thing.mqtt_password:
        return thing.mqtt_password
    else:
        return "-"


def generate_sftp_uri():
    sftp_port = os.environ.get("MINIO_SFTP_PORT").split(":")[-1]
    proxy = os.environ.get("PROXY_URL").split("://")[-1].split(":")[0]
    return f"sftp://{proxy}:{sftp_port}"


def get_active_parser(thing: Thing):
    try:
        return thing.parser.first()
    except Parser.DoesNotExist:
        return None


def get_random_chars(length: int):
    chars = string.ascii_letters + string.digits
    return "".join(secrets.choice(chars) for _ in range(length))


def create_db_username(group: Group, readonly: bool = False):
    group_name = get_group_name(group)
    vo_name = get_group_vo(group)
    name = f"{vo_name[:10]}_{group_name}"
    if readonly:
        name = "ro_" + name
    return re.sub("[^a-z0-9_]+", "", f"{name[0:30].lower()}_{uuid.uuid4()}")


def get_group_name_with_vo_prefix(group: Group) -> str:
    vo = get_group_vo(group)
    group_name = get_group_name(group)
    return f"{vo}:{group_name}"


def get_group_name(group: Group) -> str:
    group_name = group.name.split(":")[-1].split("#")[0]
    return group_name


def get_group_vo(group: Group) -> str:
    if ":group:" in group.name:
        prefix, rest_content = group.name.split(":group:", 1)
        if ":" in rest_content:
            vo, rest = rest_content.split(":", 1)
            return vo
    return ""


def filter_group_queryset_in_allowed_vo(queryset):
    # Retrieve the allowed VOs from the environment variable
    allowed_vos = os.environ.get("ALLOWED_VOS", "").split(",")
    # Remove leading and trailing spaces from each item in the list
    allowed_vos = [vo.strip() for vo in allowed_vos]
    groups_in_allowed_vos = []
    for group in queryset:
        vo = get_group_vo(group)
        if vo and vo in allowed_vos:
            groups_in_allowed_vos.append(group)
    # Convert the filtered list back to a queryset
    filtered_queryset = Group.objects.filter(
        pk__in=[group.pk for group in groups_in_allowed_vos]
    )
    return filtered_queryset


def create_bucket_name(thing: Thing):
    group_name = get_group_name(thing.group)
    vo_name = get_group_vo(thing.group)
    name = f"{vo_name[:10]}-{group_name}"
    return re.sub("[^a-z0-9-]+", "", f"{name[:26].lower()}-{thing.thing_id}")


def get_http_api_properties(thing: Thing):
    api: HttpApi = get_current_http_api_by_thing(thing)

    if not api:
        return {
            "type": None,
            "enabled": None,
            "sync_interval": None,
            "settings": None,
        }

    return {
        "type": api.get_type_name(),
        "enabled": api.sync_enabled,
        "sync_interval": api.sync_interval,
        "settings": api.get_properties_json(),
    }


def get_parser_properties(thing: Thing):
    thing_parsers: [Parser] = list(Parser.objects.get_queryset().filter(thing=thing))
    thing_active_parser: Parser = get_active_parser(thing)
    thing_active_parser_index = (
        thing_parsers.index(thing_active_parser)
        if thing_active_parser in thing_parsers
        else None
    )
    thing_parsers_json = []

    for thing_parser in thing_parsers:
        thing_parser_settings = {}
        thing_parser_type_name = (
            thing_parser.type.name if thing_parser.type is not None else None
        )

        if hasattr(thing_parser, "csvparser"):
            csvparser: CsvParser = thing_parser.csvparser
            thing_parser_settings = csvparser.get_properties_json()

        thing_parsers_json.append(
            {
                "type": thing_parser_type_name,
                "name": thing_parser.name,
                "settings": thing_parser_settings,
            }
        )

    parser_properties = {
        "default": thing_active_parser_index,
        "parsers": thing_parsers_json,
    }

    return parser_properties


def get_qaqc_properties(thing):
    window = thing.qaqc_ctx_window or ""
    if window.isnumeric():
        window = int(window)
    tests = thing.qaqc_tests or []

    assert isinstance(window, (str, int))
    assert isinstance(tests, list)
    return {
        "default": 0,
        "configs": [
            {
                # QAQC-Config protocol version
                # Increase version for any modification,
                # especially if a KEY is modified, added
                # or deleted.
                "version": 1,
                "type": "SaQC",
                "name": "MyConfig",
                "context_window": window or 0,
                "tests": tests,
            },
        ],
    }


def get_thing_properties(thing: Thing):
    storage: RawDataStorage = get_storage_by_thing(thing)
    db: Database = get_db_by_thing(thing)

    config = {
        # Config protocol version
        # Increase version for any modification, especially
        # if a KEY is modified, added or deleted.
        "version": 6,
        "uuid": str(thing.thing_id),
        "name": thing.name,
        "description": thing.description,
        "project": {
            "name": get_group_name_with_vo_prefix(thing.group),
            "uuid": str(uuid.UUID(int=thing.group.id)),
        },
        "ingest_type": thing.datasource_type,
        "database": {
            "username": db.username,
            "password": encrypt_pw(db.password),
            "url": get_connection_string_secure(thing),
            "ro_username": db.ro_username,
            "ro_password": encrypt_pw(db.ro_password),
            "ro_url": get_connection_string_secure(thing, readonly=True),
            "schema": db.username,
        },
        "qaqc": get_qaqc_properties(thing),
        "parsers": get_parser_properties(thing),
        "mqtt_device_type": (
            thing.mqtt_device_type.name if thing.mqtt_device_type else None
        ),
        "raw_data_storage": {
            "bucket_name": storage.bucket,
            "username": storage.access_key,
            "password": encrypt_pw(storage.secret_key),
            "filename_pattern": thing.sftp_filename_pattern,
        },
        "mqtt": {
            "username": thing.mqtt_username,
            "password_hash": thing.mqtt_hashed_password,
            "password": encrypt_pw(thing.mqtt_password),
            "topic": thing.mqtt_topic,
            "uri": thing.mqtt_uri,
        },
        "external_sftp": {
            "sync_enabled": thing.ext_sftp_sync_enabled,
            "uri": thing.ext_sftp_uri,
            "path": thing.ext_sftp_path,
            "username": thing.ext_sftp_username,
            "password": encrypt_pw(thing.ext_sftp_password),
            "sync_interval": thing.ext_sftp_sync_interval,
            "public_key": thing.ext_sftp_public_key,
            "private_key": encrypt_pw(
                get_ssh_privkey(f"sftp-private-keys/{thing.thing_id}")
            ),
        },
        "external_api": get_http_api_properties(thing),
    }

    return config


def get_qaqc_setting_properties(qaqc_setting: QaqcSetting):

    config = {
        # Config protocol version
        # Increase version for any modification, especially
        # if a KEY is modified, added or deleted.
        "version": 3,
        "saqc_version": os.environ.get("QAQC_SETTING_SAQC_VERSION", None),
        "default": qaqc_setting.active,
        "project_uuid": str(uuid.UUID(int=qaqc_setting.group.id)),
        "id": str(qaqc_setting.id),
        "name": qaqc_setting.name,
        "context_window": qaqc_setting.context_window,
        "functions": get_qaqc_test_properties_by_qaqc_setting(qaqc_setting),
    }

    return config


def publish_qaqc_setting_data_parsed(qaqc_setting: QaqcSetting, start_date, end_date):
    publish_qaqc_setting_data_parsed_config(
        get_qaqc_setting_data_parsed_properties(qaqc_setting, start_date, end_date)
    )


def publish_tsystems_historic_data(thing_uuid, start_date, end_date):
    publish_tsystems_historic_data_config(
        get_publish_tsystems_historic_data_properties(thing_uuid, start_date, end_date)
    )


def get_publish_tsystems_historic_data_properties(thing_uuid, start_date, end_date):
    config = {
        "thing": str(thing_uuid),
        "datetime_from": start_date.strftime("%Y-%m-%dT%H:%M:%SZ"),
        "datetime_to": end_date.strftime("%Y-%m-%dT%H:%M:%SZ"),
    }

    return config


def get_qaqc_setting_data_parsed_properties(
    qaqc_setting: QaqcSetting, start_date, end_date
):
    config = {
        # Config protocol version
        # Increase version for any modification, especially
        # if a KEY is modified, added or deleted.
        "version": 2,
        "project_uuid": str(uuid.UUID(int=qaqc_setting.group.id)),
        "qc_settings_name": qaqc_setting.name,
        "start_date": start_date.isoformat(),
        "end_date": end_date.isoformat(),
    }

    return config


def get_qaqc_test_properties_by_qaqc_setting(qaqc_setting: QaqcSetting):
    qaqc_tests = QaqcTest.objects.filter(qaqc_setting=qaqc_setting)
    qaqc_test_properties = []
    for qaqc_test in qaqc_tests:
        qaqc_function = get_current_qaqc_function_by_qaqc_test(qaqc_test)
        if not qaqc_function:
            continue

        qaqc_test_properties.append(
            {
                "name": qaqc_test.name,
                "func_id": qaqc_function.get_function_name(),
                "kwargs": qaqc_function.get_properties_json(),
                "datastreams": get_datastreams_properties_by_qaqc_test(qaqc_test),
            }
        )
    return qaqc_test_properties


def get_datastreams_properties_by_qaqc_test(qaqc_test: QaqcTest):
    datastreams = []

    field_json = json.loads(qaqc_test.field.replace("'", ""))
    target_json = json.loads(qaqc_test.target.replace("'", ""))

    for datastream in field_json:
        datastreams.append(
            {
                "arg_name": "field",
                "sta_thing_id": (datastream["thing"] if datastream["thing"] else None),
                "sta_stream_id": (
                    datastream["datastream"] if datastream["thing"] else None
                ),
                "alias": (
                    f'T{datastream["thing"]}S{datastream["datastream"]}'
                    if datastream["thing"]
                    else datastream["datastream"]
                ),
            }
        )
    for datastream in target_json:
        datastreams.append(
            {
                "arg_name": "target",
                "sta_thing_id": (datastream["thing"] if datastream["thing"] else None),
                "sta_stream_id": (
                    datastream["datastream"] if datastream["thing"] else None
                ),
                "alias": (
                    f'T{datastream["thing"]}S{datastream["datastream"]}'
                    if datastream["thing"]
                    else datastream["datastream"]
                ),
            }
        )
    return datastreams


def custom_clean(self, cleaned_data):
    if self.instance.id:
        # when editing a thing, the group of the thing gets added during the save process as it is readonly
        cleaned_data["group"] = self.instance.group

    fields = ["datasource_type"]

    if cleaned_data.get("datasource_type"):
        fields = {
            "SFTP": [
                "qaqc_ctx_window",
                "sftp_filename_pattern",
                "parser",
                "parser_type",
            ],
            "MQTT": ["qaqc_ctx_window", "mqtt_uri", "mqtt_topic", "mqtt_device_type"],
            "extSFTP": [
                "qaqc_ctx_window",
                "sftp_filename_pattern",
                "parser",
                "parser_type",
                "ext_sftp_uri",
                "ext_sftp_path",
                "ext_sftp_username",
                "ext_sftp_sync_interval",
            ],
            "extAPI": [
                "qaqc_ctx_window",
            ],
        }[cleaned_data.get("datasource_type")]

    for field in fields:
        if cleaned_data.get(field):
            continue
        self.add_error(field, "This field could not be empty.")

    # validate qaqc
    obj = cleaned_data.get("qaqc_tests")
    if obj is not None and not isinstance(obj, list):
        self.add_error("qaqc_tests", f"Expected a JSON array.")

    # validate sync interval
    obj = self.cleaned_data.get("ext_sftp_sync_interval")
    if obj is not None:
        if obj < 10:
            self.add_error(
                "ext_sftp_sync_interval", f"Minimum sync interval is 10 minutes."
            )
        # TODO: maximum sync interval?


def generate_keypair(filename):
    private_key_filename = os.path.join("sftp-private-keys", filename)

    key = paramiko.RSAKey.generate(2048)
    key.write_private_key_file(private_key_filename)
    public_key = f"{key.get_name()} {key.get_base64()}"

    return private_key_filename, public_key


def get_time_with_unit_by_time_in_minutes(time_in_minutes):
    if not time_in_minutes:
        return None, None

    value = int(time_in_minutes)
    if value % 60 == 0:
        hours = value / 60
        if hours % 24 == 0:
            value, unit = int(hours / 24), "d"
        else:
            value, unit = int(hours), "h"
    else:
        value, unit = value, "min"
    return value, unit


def get_encryption_secret():
    key = os.environ.get("FERNET_ENCRYPTION_SECRET", None)
    if not key:
        raise EnvironmentError("missing environment variable FERNET_ENCRYPTION_SECRET")
    return key


def encrypt_pw(pw):
    if not pw:
        return pw
    key = get_encryption_secret()
    return Fernet(key).encrypt(pw.encode()).decode()


def decrypt_pw(encrypted_pw):
    if not encrypted_pw:
        return encrypted_pw
    key = get_encryption_secret()
    return Fernet(key).decrypt(encrypted_pw.encode()).decode()


def get_ssh_privkey(file) -> str:
    # to wrap the private key back to a paramiko.RSAKey use:
    # paramiko.RSAKey.from_private_key(io.StringIO(priv_key))
    if not os.path.exists(file):
        return ""
    with open(file, "r") as f:
        return f.read()
