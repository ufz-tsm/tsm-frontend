from django.contrib import admin

from .models import (
    Thing,
    CsvParser,
    QaqcSetting,
)
from .models.MqttDeviceType import MqttDeviceType
from .models.NmStation import NmStation

from .admin_classes.thingAdmin import ThingAdmin
from .admin_classes.qaqcSettingAdmin import QaqcSettingAdmin
from .admin_classes.csvParserAdmin import CsvParserAdmin
from .admin_classes.mqttDeviceTypeAdmin import MqttDeviceTypeAdmin
from .admin_classes.nmStationAdmin import NmStationAdmin

admin.site.register(Thing, ThingAdmin)
admin.site.register(QaqcSetting, QaqcSettingAdmin)
admin.site.register(MqttDeviceType, MqttDeviceTypeAdmin)
admin.site.register(CsvParser, CsvParserAdmin)
admin.site.register(NmStation, NmStationAdmin)
admin.site.enable_nav_sidebar = False
