from django.apps import AppConfig


class MainConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "tsm"
    verbose_name = "time.IO"

    def ready(self):
        # Implicitly connect signal handlers decorated with @receiver.
        from . import signals
