import os

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import pre_save, post_save, m2m_changed, post_migrate
from django.dispatch import receiver
from django_helmholtz_aai.signals import aai_user_logged_in, aai_vo_created

from tsm.models import (
    Thing,
    Database,
    RawDataStorage,
    CsvParser,
    ParserType,
    Parser,
    HttpApi,
    QaqcSetting,
    QaqcTest,
)
from tsm.mqtt_actions import (
    publish_thing_config,
    publish_user_groups,
    publish_qaqc_setting_config,
)
from tsm.utils import (
    create_bucket_name,
    get_random_chars,
    get_db_by_thing,
    create_db_username,
    get_storage_by_thing,
    generate_sftp_uri,
    get_thing_properties,
    generate_keypair,
    get_qaqc_setting_properties,
)


def subclass_receiver(signal, superclass, **kwargs):
    """
    A decorator for connecting signals that are sent by any subclass of a given superclass
    :param signal: The signal to connect
    :param superclass: The superclass of senders to connect
    """

    def decorator(receiver_func):
        for subclass_sender in superclass.__subclasses__():
            signal.connect(receiver_func, sender=subclass_sender, **kwargs)
        return receiver_func

    return decorator


@receiver(pre_save, sender=Thing)
def add_mqtt_password_hash(sender, instance, **kwargs):
    thing = instance
    if thing.mqtt_username is None:
        thing.mqtt_username = create_bucket_name(thing)
    if thing.mqtt_password is None:
        thing.mqtt_password = get_random_chars(40)
        thing.mqtt_hashed_password = make_password(thing.mqtt_password, hasher="PBKDF2")
    if not thing.ext_sftp_public_key and thing.datasource_type == "extSFTP":
        (private_key_file, public_key) = generate_keypair(str(thing.thing_id))
        thing.ext_sftp_public_key = public_key


@receiver(post_save, sender=Thing)
def process_thing(sender, instance, created, **kwargs):
    thing = instance
    database = get_db_by_thing(thing)

    if database is None:
        database = Database()
        if "POSTGRES_PORT" in os.environ:
            database.url = (
                f"{os.environ.get('POSTGRES_HOST')}:{os.environ.get('POSTGRES_PORT')}"
            )
        else:
            database.url = os.environ.get("POSTGRES_HOST")
        database.name = os.environ.get("POSTGRES_NAME")
        database.username = create_db_username(thing.group)
        database.password = get_random_chars(24)
        database.ro_username = create_db_username(thing.group, readonly=True)
        database.ro_password = get_random_chars(24)
        database.group = thing.group
        database.save()

    # only needed for things that were created before including the readonly user
    if database.ro_username is None:
        database.ro_username = create_db_username(thing.group, readonly=True)
        database.ro_password = get_random_chars(24)
        database.save()

    if get_storage_by_thing(thing) is None:
        name = create_bucket_name(thing)

        storage = RawDataStorage()
        storage.bucket = name
        storage.access_key = name
        storage.secret_key = get_random_chars(40)
        storage.fileserver_uri = generate_sftp_uri()
        storage.thing = thing
        storage.save()

    if hasattr(thing, "m2m_will_change"):
        del thing.m2m_will_change
        return

    publish_thing(thing)


@receiver(post_save, sender=QaqcSetting)
def process_qaqc_setting(sender, instance, created, **kwargs):
    qaqc_setting = instance
    qaqc_tests = QaqcTest.objects.filter(qaqc_setting=qaqc_setting)
    if len(qaqc_tests) == 0:
        return

    publish_qaqc_setting_config(get_qaqc_setting_properties(qaqc_setting))


@receiver(m2m_changed, sender=Thing.parser.through)
def thing_parsers_changed(
    sender, instance: Thing, action, reverse, model, pk_set, **kwargs
):
    if action == "post_add" and instance.datasource_type in ["SFTP", "extSFTP"]:
        publish_thing(instance)


@subclass_receiver(post_save, superclass=HttpApi)
def thing_api_changed(sender, instance: HttpApi, **kwargs):
    publish_thing(instance.thing)


@receiver(post_save, sender=CsvParser)
def create_or_associate_parsertype(sender, instance: CsvParser, created, **kwargs):
    if created:
        parsertype, created = ParserType.objects.get_or_create(name="CsvParser")
        parser: Parser = Parser.objects.get(pk=instance.parser_ptr_id)
        parser.type = parsertype
        parser.save()


@subclass_receiver(post_save, superclass=Parser)
def publish_related_things(sender, instance: Parser, created, **kwargs):
    if not created:
        things_using_parser = instance.thing_set.all()
        for thing in things_using_parser:
            publish_thing(thing)


def publish_thing(thing):
    # create or update thing in the respective database
    publish_thing_config(get_thing_properties(thing))


@receiver(aai_user_logged_in)
def extend_django_user(user, userinfo, **kwargs):
    user.is_staff = True
    user.save()


@receiver(aai_user_logged_in)
def publish_groups_on_login(userinfo, **kwargs):
    publish_user_groups(userinfo)


@receiver(aai_vo_created)
def add_group_permissions(vo, **kwargs):
    content_types = ContentType.objects.filter(app_label="tsm")
    permissions = Permission.objects.filter(content_type__in=content_types)
    vo.permissions.set(permissions)


@receiver(post_migrate)
# Ensure that all necessary permissions are granted to all groups
def grant_permissions(sender, **kwargs):
    for group in Group.objects.all():
        add_group_permissions(group)
