import os
from json import JSONDecodeError

from django.shortcuts import render
from django.http import JsonResponse
from requests import RequestException

from .models import Parser, Database

import requests
import json


# Create your views here.
def about(request):
    return render(request, "admin/about.html")


def get_parser_objects(request):
    user_groups = request.user.groups.all()

    parser_type_id = request.GET.get("parser_type_id")
    group_id = request.GET.get("group_id")

    # Modify this line to filter Parser objects based on the user's groups
    parser_objects = Parser.objects.filter(
        type_id=parser_type_id, group_id=group_id
    ).values("id", "name")

    return JsonResponse(list(parser_objects), safe=False)


def get_things_and_datastreams_from_sta(request):

    def empty_response():
        return JsonResponse([], safe=False)

    group_id = request.GET.get("group_id")
    if not group_id:
        return empty_response()

    databases = Database.objects.filter(group_id=group_id)
    if not databases or len(databases) != 1:
        return empty_response()

    database = databases[0]
    database_name = database.username

    sta_root_url = os.environ.get("STA_ROOT_URL")
    if not sta_root_url:
        return empty_response()
    sta_url = f"{sta_root_url}/{database_name}/v1.1/Things?$select=@iot.id,name&$expand=Datastreams($select=@iot.id,name)"

    try:
        res = requests.get(sta_url)
        if not res.ok:
            return empty_response()
    except RequestException:
        return empty_response()

    try:
        response = json.loads(res.text)
    except JSONDecodeError:
        return empty_response()

    return JsonResponse(response.get("value", []), safe=False)
