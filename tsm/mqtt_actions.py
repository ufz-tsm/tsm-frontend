from __future__ import annotations

import logging
import os
import json
import socket
import sys
import time
from threading import get_native_id

import paho.mqtt.client as mqtt

logger = logging.getLogger("django")

client: mqtt.Client | None = None


def on_connect(client, userdata, flags, reason_code, properties=None):
    logger.info(
        f"Connected to MQTT broker {(userdata['mqtt_config']).get('broker_host')} "
        f"port {(userdata['mqtt_config']).get('broker_port')}"
    )


def on_publish(client, userdata, mid, result=None, properties=None):
    logger.info("Message with id: {} published.".format(mid))


mqtt_config = {
    "broker_host": os.environ.get("MQTT_BROKER_HOST"),
    "broker_port": int(os.environ.get("MQTT_BROKER_PORT", 1883)),
    "client_id": os.environ.get("MQTT_CLIENT_ID", "frontend"),
    "user": os.environ.get("MQTT_USER"),
    "password": os.environ.get("MQTT_PASSWORD"),
}


def mk_client():
    global client
    # We need a unique client id for each frontend thread/process.
    # Otherwise, we get a race-condition and each new client dis-
    # connects its predecessor.
    prefix = f"-{get_native_id()}"
    client = mqtt.Client(
        protocol=mqtt.MQTTv5,
        client_id=mqtt_config["client_id"] + prefix,
        userdata={"mqtt_config": mqtt_config},
    )
    client.username_pw_set(mqtt_config["user"], mqtt_config["password"])
    client.on_connect = on_connect
    client.on_publish = on_publish

    try:
        client.connect(host=mqtt_config["broker_host"], port=mqtt_config["broker_port"])
    except (socket.gaierror, ConnectionRefusedError) as e:
        raise ConnectionError(
            f"Unable to connect to mqtt broker {mqtt_config['broker_host']} "
            f"on port {mqtt_config['broker_port']}"
        ) from e

    # Spawn an own client thread, which handle
    # ACKs from broker when using QOS>0.
    client.loop_start()


def publish_thing_config(thing_event_msg_json):
    config = json.dumps(thing_event_msg_json)

    if client is None:
        mk_client()
        time.sleep(1)

    if client.is_connected():
        result = client.publish("frontend_thing_update", str(config), qos=2)
        result.wait_for_publish(5)
        logger.info(
            f"thing published on 'frontend_thing_update': {thing_event_msg_json}"
        )
    else:
        raise Exception(
            f"Unable to publish mqtt message to broker at {mqtt_config['broker_host']}."
            f" Client not connected."
        )


def publish_qaqc_setting_config(qaqc_event_msg_json):
    config = json.dumps(qaqc_event_msg_json)

    if client is None:
        mk_client()
        time.sleep(1)

    if client.is_connected():
        result = client.publish("qaqc_settings_update", str(config), qos=2)
        result.wait_for_publish(5)
        logger.info(
            f"QA/QC setting published on 'qaqc_settings_update': {qaqc_event_msg_json}"
        )
    else:
        raise ConnectionError(
            f"Unable to publish mqtt message to broker at {mqtt_config['broker_host']}."
            f" Client not connected."
        )


def publish_qaqc_setting_data_parsed_config(qaqc_event_msg_json):
    config = json.dumps(qaqc_event_msg_json)

    if client is None:
        mk_client()
        time.sleep(1)

    if client.is_connected():
        result = client.publish("data_parsed", str(config), qos=2)
        result.wait_for_publish(5)
        logger.info(f"Published on 'data_parsed': {qaqc_event_msg_json}")
    else:
        raise ConnectionError(
            f"Unable to publish mqtt message to broker at {mqtt_config['broker_host']}."
            f" Client not connected."
        )


def publish_tsystems_historic_data_config(json_data):
    config = json.dumps(json_data)

    topic = "sync_ext_apis/tsystems"

    if client is None:
        mk_client()
        time.sleep(1)

    if client.is_connected():
        result = client.publish(topic, str(config), qos=2)
        result.wait_for_publish(5)
        logger.info(f"Published on '{topic}': {json_data}")
    else:
        raise ConnectionError(
            f"Unable to publish mqtt message to broker at {mqtt_config['broker_host']}."
            f" Client not connected."
        )


def publish_user_groups(userinfo):
    keys = ["eduperson_principal_name", "eduperson_entitlement"]
    # reduce published userinfo to keys: eduperson_principal_name, eduperson_entitlement
    user_groups = json.dumps({key: userinfo.get(key) for key in keys})

    if client is None:
        mk_client()
        time.sleep(1)

    if not client.is_connected():
        raise ConnectionError(
            f"Unable to publish mqtt message to broker at "
            f"{mqtt_config['broker_host']}, because client "
            f"is not connected."
        )

    # publish email and eduperson_entitlement to topic user_login
    # access to topic is granted to FRONTEND_MQTT_USER in mosquitto/docker-entrypoint.sh script
    result = client.publish("user_login", str(user_groups), qos=2)
    result.wait_for_publish(5)
    logger.info(f"User groups published: {user_groups}")


# Do not try to connect the mqtt broker when using the Django cli (manage.py),
# except when the server mode (`runserver` in development environments) is started
if client is None and "manage.py" not in sys.argv[0] or "runserver" in sys.argv:
    mk_client()
