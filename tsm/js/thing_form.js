function toggleIngestTypeFieldsets(ingestTypes, value) {
    for (const [ingestType, ingestTypeFields] of Object.entries(ingestTypes)) {
        Array.from(ingestTypeFields).forEach(field => field.style.display = 'none')
    }
    for (const [ingestType, ingestTypeFields] of Object.entries(ingestTypes)) {
        if (ingestType != value) continue
        Array.from(ingestTypeFields).forEach(field => field.style.display = 'block')
    }

    if (value == "extAPI") {
        initDynamicHttpApiFields()
    }
}

function toggleHttpApiFieldsets(fields, value) {
    for (const [api_type, api_fields] of Object.entries(fields)) {
        Array.from(api_fields).forEach(field => field.style.display = 'none')
    }
    for (const [api_type, api_fields] of Object.entries(fields)) {
        if (api_type != value) continue
        Array.from(api_fields).forEach(field => field.style.display = 'block')
    }
}

function initDynamicFields() {
    initDynamicHttpApiFields()
    initDynamicIngestTypeField()
}

function initDynamicHttpApiFields() {
    const field = document.getElementById("id_http_api_type")

    if (field === null) {
        return
    }

    const apis = {
        ttn: document.getElementsByClassName("http-api-ttn"),
        dwd: document.getElementsByClassName("http-api-dwd"),
        nm: document.getElementsByClassName("http-api-nm"),
        bosch: document.getElementsByClassName("http-api-bosch"),
        uba: document.getElementsByClassName('http-api-uba'),
        tsystems: document.getElementsByClassName('http-api-tsystems')
    }

    field.addEventListener('change', function () {
        toggleHttpApiFieldsets(apis, field.value)
    })

    toggleHttpApiFieldsets(apis, field.value)
}

function initDynamicIngestTypeField() {
    const field = document.getElementById("id_datasource_type")

    if (field === null) {
        return
    }

    const ingestTypes = {
        SFTP: document.getElementsByClassName("sftp-settings"),
        MQTT: document.getElementsByClassName("mqtt-settings"),
        extSFTP: document.getElementsByClassName("ext-sftp-settings"),
        extAPI: document.getElementsByClassName("http-api-settings"),
    }


    field.addEventListener('change', function () {
        toggleIngestTypeFieldsets(ingestTypes, field.value)
    })

    toggleIngestTypeFieldsets(ingestTypes, field.value)
}

jQuery(document).ready(function ($) {
    initDynamicFields();

    $('#id_group').change(function () {
        $("#id_parser_type").val("");
        $('#id_parser').empty()
    })

    $('#id_parser_type').change(function () {
        let parserField = $('#id_parser')
        let parser_type_id = $(this).val()
        let group_id = $('#id_group').val() || $('#id_group_1').val() // id_group_1 is in edit/change form of thing
        if (!group_id) {
            alert('You must select a project first!')
            $("#id_parser_type").val("");
            return
        }
        if (parser_type_id && group_id) {
            $.ajax({
                url: '/frontend/tsm/get_parser_objects/',
                data: { 'parser_type_id': parser_type_id, 'group_id': group_id },
                success: function (data) {
                    parserField.empty();
                    parserField.append($('<option>', {
                        value: "",
                        text: "---------"
                    }))

                    $.each(data, function (index, element) {
                        parserField.append($('<option>', {
                            value: element.id,
                            text: element.name
                        }))
                    })
                }
            })
        } else {
            parserField.empty();
        }
    })

    // CSS workarounds for styling embedded form
    $("h3").css("display", "none");
    $(".inline-group h2").css("text-transform", "none");
    $(".js-inline-admin-formset.inline-group").insertAfter(".http-api-settings:first").removeClass();


    // ensure that readonly fields do not overflow
    $("div.readonly").addClass("form-row")
});
