const AVAILABLE_FUNCTIONS = ["freetext"]
const FORM_FIELDS = ["field", "target"]

function showFields(formIndex, functionKeyToShow) {
    for (const functionKey of AVAILABLE_FUNCTIONS) {
        const element = document.querySelector(`div[id*=qaqctest_set-${formIndex}] .qaqc-test-function-${functionKey}`);
        if (!element) continue;
        element.style.display = functionKey === functionKeyToShow ? 'block' : 'none';
    }
}

function initDynamicFunctionForms() {
    const selects = document.querySelectorAll('.field-qaqc_function_type select[multiple]');

    selects.forEach(function (select, i) {
        select.addEventListener('change', function () {
            showFields(i, select.value);
        });
        showFields(i, select.value);
    });
}

let initialDataWasFilled = false
let staDataForSelects = []
let currentlySelectedThingIds = {"field": [], "target": []}

jQuery(document).ready(function ($) {
    // Handler for "Add another Qaqc Test" button
    $('.add-item .add-handler').click(addFieldAndTargetForNewForm)

    // Handler for group selection
    $('#id_group').change(function () {
        const groupId = $('#id_group').val();
        if (!groupId) {
            return
        }

        // Fetch STA
        $.ajax({
            url: '/frontend/tsm/get_things_and_datastreams_from_sta/',
            data: { 'group_id': groupId },
            success: initFieldAndTargetSelects,
        });
    });

    initDynamicFunctionForms();
});

function initFieldAndTargetSelects(staData) {
    staDataForSelects = [...staData, getStaDataEntryForTemporaryThing()]

    for (const field of FORM_FIELDS) {
        const selectElements = getSelectElementsByFieldName(field)
        const thingSelectElements = getThingSelectElementsFromSelectElementList(selectElements)
        const datastreamSelectElements = getDatastreamSelectElementsFromSelectElementList(selectElements)
        const containerElements = getContainerElementsByFieldName(field).slice(0, -1)

        createTemporaryThingOptionsForEveryField()
        createAndFillFieldsForContext(field, thingSelectElements, datastreamSelectElements)

        if (!initialDataWasFilled) {
            addDatastreamInputFields(containerElements)
        }
    }
    initialDataWasFilled = true
}

function addFieldAndTargetForNewForm() {
    setTimeout(function () {
        initDynamicFunctionForms()
        for (let field of FORM_FIELDS) {
            const selectElements = getSelectElementsByFieldName(field)
            const thingSelectElements = getThingSelectElementsFromSelectElementList(selectElements).slice(-2, -1)
            const datastreamSelectElements = getDatastreamSelectElementsFromSelectElementList(selectElements).slice(-2, -1)
            const containerElements = getContainerElementsByFieldName(field).slice(-2, -1)
            createAndFillFieldsForContext(field, thingSelectElements, datastreamSelectElements)
            addDatastreamInputFields(containerElements)
        }
    }, 10);
}

function createAndFillFieldsForContext(field, thingSelectElements, datastreamSelectElements) {
    clearInputs()

    thingSelectElements.forEach(function (thingSelectElement, selectElementIndex) {
        currentlySelectedThingIds[field][selectElementIndex] = []

        createThingOptionElementsAndDatastreamElementsByStaThing()
        thingSelectElement.removeEventListener('change', thingSelectEventListener);
        thingSelectElement.addEventListener('change', thingSelectEventListener);

        if (!initialDataWasFilled) {
            fillInitialData()
        }

        function fillInitialData() {
            const selectionMappedByThing = getCurrentSelectionMappedByThing()

            for (const thingDatastreamsTuple of selectionMappedByThing) {
                const selectedThingId = thingDatastreamsTuple["thing"]
                const selectedDatastreamIds = thingDatastreamsTuple["datastreams"]

                findThingOptionElementByThingId(selectedThingId).selected = true
                currentlySelectedThingIds[field][selectElementIndex].push(selectedThingId)

                const staDatastreamsToShow = getStaDatastreamsByThingId(selectedThingId)
                const staDatastreamsToSelect = staDatastreamsToShow.filter(ds => selectedDatastreamIds.includes(getIdByStaEntity(ds)))

                const datastreamOptionElementsToShow = findDatastreamOptionElementsByDatastreams(staDatastreamsToShow)
                const datastreamOptionElementsToSelect = findDatastreamOptionElementsByDatastreams(staDatastreamsToSelect)
                datastreamOptionElementsToShow.forEach(showDatastreamOptionElement)
                datastreamOptionElementsToSelect.forEach(selectAndShowDatastreamOptionElement)
            }
        }

        function thingSelectEventListener(event) {

            let selectedOptions = Array.from(event.target.selectedOptions).map(option => option.value);
            let addedThingIds = selectedOptions.filter(value => !currentlySelectedThingIds[field][selectElementIndex].includes(value));
            let removedThingIds = currentlySelectedThingIds[field][selectElementIndex].filter(value => !selectedOptions.includes(value));

            addedThingIds.forEach(selectDatastreamsByThingId);
            removedThingIds.forEach(unselectDatastreamsByThingId);

            currentlySelectedThingIds[field][selectElementIndex] = selectedOptions;
        }

        function createThingOptionElementsAndDatastreamElementsByStaThing() {
            const datastreamSelectElement = datastreamSelectElements[selectElementIndex];
            staDataForSelects.forEach(function (thing) {
                thingSelectElement.appendChild(createThingOptionElement(thing))
                const datastreams = thing["Datastreams"]
                datastreams.forEach(function (datastream) {
                    const opt = createDatastreamOptionElementByThingAndDatastream(thing, datastream)
                    hideDatastreamOptionElement(opt)
                    datastreamSelectElement.appendChild(opt);
                })
            });
        }

        function getCurrentSelectionMappedByThing() {
            const currentSelectionElement = findCurrentSelectionTextareaElement()
            if (!currentSelectionElement) return []

            const currentSelectionListString = currentSelectionElement.innerHTML.replaceAll("\'", "")
            const currentSelectionList = currentSelectionListString ? JSON.parse(currentSelectionListString) : []

            const selectionMappedByThing = [];
            currentSelectionList.forEach(item => {
                const existingThing = selectionMappedByThing.find(entry => entry.thing === item.thing);
                if (existingThing) {
                    existingThing.datastreams.push(item.datastream);
                } else {
                    selectionMappedByThing.push({ thing: item.thing, datastreams: [item.datastream] });
                }
            });

            return selectionMappedByThing
        }

        function getFieldIdentifier() {
            const fieldIndex = parseInt(thingSelectElement.id.split("-")[1])
            return fieldIndex ?? null
        }

        function findDatastreamOptionElementsByDatastreams(datastreams) {
            const allElements = []
            for (const datastream of datastreams) {
                const datastreamId = datastream["@iot.id"]
                if (!datastreamId) { continue }
                const datastreamElements = document.querySelectorAll(`#qaqctest_set-${getFieldIdentifier()} fieldset .field-${field} select option[value*='"datastream":"${datastreamId}"']`)
                datastreamElements.forEach(e => allElements.push(e))
            }
            return allElements
        }

        function findCurrentSelectionTextareaElement() {
            return document.querySelector(`#qaqctest_set-${getFieldIdentifier()} fieldset .field-current_${field} textarea`)
        }

        function findThingOptionElementByThingId(thingId) {
            return document.querySelector(`#qaqctest_set-${getFieldIdentifier()} fieldset .field-${field} select option[value="${thingId}"]`)
        }

        function hideDatastreamOptionElement(element) {
            element.style.display = "none"
            element.selected = false
        }

        function selectAndShowDatastreamOptionElement(element) {
            element.selected = true
            showDatastreamOptionElement(element)
        }

        function showDatastreamOptionElement(element) {
            element.style.display = "block"
        }

        function selectDatastreamsByThingId(addedThingId) {
            const datastreamsToShow = getStaDatastreamsByThingId(addedThingId)
            const datastreamOptionElementsToShow = findDatastreamOptionElementsByDatastreams(datastreamsToShow)
            datastreamOptionElementsToShow.forEach(showDatastreamOptionElement)
        }

        function unselectDatastreamsByThingId(removedThingId) {
            const datastreamsToRemove = getStaDatastreamsByThingId(removedThingId)
            const datastreamOptionElementsToRemove = findDatastreamOptionElementsByDatastreams(datastreamsToRemove)
            datastreamOptionElementsToRemove.forEach(hideDatastreamOptionElement)
        }
    });

    function clearInputs() {
        thingSelectElements.forEach(f => f.innerHTML = '')
        datastreamSelectElements.forEach(f => f.innerHTML = '')
    }
}

function createThingOptionElement(thing) {
    const opt = document.createElement('option');
    opt.value = thing["@iot.id"].toString();
    opt.innerHTML = thing["name"];
    return opt
}

function createDatastreamOptionElementByThingAndDatastream(thing, datastream) {
    const opt = document.createElement('option');
    opt.value = JSON.stringify({
        "thing": thing["@iot.id"].toString(),
        "datastream": datastream["@iot.id"].toString()
    });
    const alias = thing["@iot.id"] === "" ? datastream["name"] : `T${thing["@iot.id"]}S${datastream["@iot.id"]}`
    const thingName = thing["@iot.id"] === "" ? "TEMP" : thing["name"]
    opt.innerHTML = `<span class="datastreamName">${thingName}/${datastream["name"]}</span><span class="datastreamAlias"> &#8226; <i>alias:</i> ${alias}</span>`;
    opt.title = datastream["name"]
    return opt
}

function addDatastreamInputFields(containerElements, thingSelectElements) {
    containerElements.forEach(function (containerElement, i) {
        const divWrapper = document.createElement('div')
        divWrapper.classList.add("field-name")

        const label = document.createElement('span')
        label.classList.add("temporaryThingLabel")
        label.textContent = "Create Datastream for:"

        const thingSelect = document.createElement('select');
        thingSelect.classList.add("temporaryThingSelectField")

        for (const thing of staDataForSelects) {
            const selectOption = document.createElement('option');
            selectOption.value = thing["@iot.id"].toString();
            selectOption.innerText = thing["name"];
            thingSelect.appendChild(selectOption);
        }

        const input = document.createElement('input');
        input.classList.add("temporaryThingTextField")
        input.placeholder = "Datastream name"

        const addButton = document.createElement("button")
        addButton.innerHTML = "+"
        addButton.type = "button"
        addButton.classList.add("temporaryThingAddButton")
        addButton.addEventListener("click", event => onTemporaryDatastreamCreation(thingSelect, input, event))

        divWrapper.appendChild(label)
        divWrapper.appendChild(thingSelect)
        divWrapper.appendChild(input)
        divWrapper.appendChild(addButton)
        containerElement.appendChild(divWrapper)
    })

    function onTemporaryDatastreamCreation(thingSelect, datastreamNameInput, event) {
        event.stopImmediatePropagation();
        if (!datastreamNameInput.value) return;
        const thingId = thingSelect.value.toString()
        if (temporaryDatastreamNameExists(thingSelect.value, datastreamNameInput.value)) return;

        const newTemporaryDatastream = createTemporaryDatastreamByName(datastreamNameInput.value)
        staDataForSelects.findLast(t => t["@iot.id"] === thingId)["Datastreams"].push(newTemporaryDatastream)

        const datastreamSelectElements = getDatastreamSelectElementsFromSelectElementList(getAllSelectElements())
        for (const j in datastreamSelectElements) {
            const opt = createDatastreamOptionElementByThingAndDatastream(getStaThingByThingId(thingId), newTemporaryDatastream)
            datastreamSelectElements[j].appendChild(opt)
        }
        datastreamNameInput.value = ""

        const allRespectiveThingOptionElements = Array.from(document.querySelectorAll(
            `fieldset .field-field select[multiple] option[value="${thingId}"], fieldset .field-target select[multiple] option[value="${thingId}"]`
        ))
        allRespectiveThingOptionElements.forEach(el => el.selected = true)
        const allRespectiveThingDatastreamOptionElements = Array.from(document.querySelectorAll(
            `fieldset .field-field select[multiple] option[value*='"thing":"${thingId}"'], fieldset .field-target select[multiple] option[value*='"thing":"${thingId}"']`
        ))
        allRespectiveThingDatastreamOptionElements.forEach(el => el.style.display = "block")
        FORM_FIELDS.forEach(
            field => currentlySelectedThingIds[field].forEach(ids => ids.push(thingId))
        )

    }
}

function getAllSelectElements() {
    return Array.from(document.querySelectorAll(`.field-field select[multiple], .field-target select[multiple]`))
}

function getSelectElementsByFieldName(fieldName) {
    return Array.from(document.querySelectorAll(`.field-${fieldName} select[multiple]`))
}

function getContainerElementsByFieldName(fieldName) {
    return Array.from(document.querySelectorAll(`.form-row.field-${fieldName} > div`))
}

function getDatastreamSelectElementsFromSelectElementList(elementList) {
    return elementList.filter((_, i) => i % 2 === 1);
}

function getThingSelectElementsFromSelectElementList(elementList) {
    return elementList.filter((_, i) => i % 2 === 0);
}

function getTemporaryThing() {
    return {
        "@iot.id": "",
        "name": "TEMP",
    }
}

function getStaDataEntryForTemporaryThing() {
    return {
        "@iot.id": "",
        "name": "TEMP",
        "Datastreams": []
    }
}

function createTemporaryDatastreamByName(name) {
    return {
        "@iot.id": name.replaceAll("'", ""),
        "name": name.replaceAll("'", "")
    }
}

function createTemporaryThingOptionsForEveryField() {
    const allSelectionElements = Array.from(document.querySelectorAll(`fieldset .field-current_field textarea, fieldset .field-current_target textarea`))

    for (const currentSelectionElement of allSelectionElements) {
        const currentSelectionListString = currentSelectionElement.innerHTML.replaceAll("\'", "")
        const currentSelectionList = currentSelectionListString ? JSON.parse(currentSelectionListString) : []
        for (const selection of currentSelectionList) {
            const respectiveThingIndex = staDataForSelects.findIndex(t => t["@iot.id"] === selection["thing"])
            if (respectiveThingIndex === -1) {
                continue
            }
            if (!staDataForSelects[respectiveThingIndex]["Datastreams"].map(ds => ds["@iot.id"]).includes(selection["datastream"])) {
                staDataForSelects[respectiveThingIndex]["Datastreams"].push({
                    "@iot.id": selection["datastream"],
                    "name": selection["datastream"]
                })
            }
        }
    }
}

function getIdByStaEntity(staThingOrDatastream) {
    return staThingOrDatastream["@iot.id"].toString()
}

function getStaThingByThingId(thingId) {
    return staDataForSelects.find(t => t["@iot.id"].toString() === thingId)
}

function getStaDatastreamsByThingId(thingId) {
    const thing = getStaThingByThingId(thingId)
    return thing?.["Datastreams"] ?? []
}

function temporaryDatastreamNameExists(thingId, datastreamName) {
    const things = getStaDatastreamsByThingId(thingId)
    return [...things.map(ds => ds["@iot.id"]), ...things.map(ds => ds["name"])].includes(datastreamName)
}