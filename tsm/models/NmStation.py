from django.db import models


class NmStation(models.Model):
    station_id = models.CharField("Station ID", max_length=5, unique=True)
    description = models.CharField("Description", max_length=255)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.station_id = self.station_id.upper()
        super(NmStation, self).save()

    def __str__(self):
        return str(self.station_id)
