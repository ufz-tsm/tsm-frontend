from django.contrib.auth.models import Group
from django.db import models


class Database(models.Model):
    url = models.CharField(max_length=1000)
    name = models.CharField(max_length=1000)
    username = models.CharField(max_length=200)
    ro_username = models.CharField(max_length=200, blank=True, null=True)
    password = models.CharField(max_length=200)
    ro_password = models.CharField(max_length=200, blank=True, null=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name="Project")

    def __str__(self):
        return str(self.username)
