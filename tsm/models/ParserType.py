from django.db import models


class ParserType(models.Model):
    name = models.CharField(max_length=1000, unique=True)

    def __str__(self):
        return str(self.name)
