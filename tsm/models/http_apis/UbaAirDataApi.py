from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi


class UbaAirDataApi(HttpApi):
    station_id = models.CharField("Station ID")
    sync_interval = models.IntegerField("Sync Interval", default=60)

    @staticmethod
    def get_type_name():
        return "uba"

    def get_properties_json(self):
        return {"version": 1, "station_id": self.station_id}
