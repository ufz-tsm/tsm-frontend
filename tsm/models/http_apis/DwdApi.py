from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi


class DwdApi(HttpApi):
    station_id = models.CharField("Station ID", max_length=5)
    sync_interval = models.IntegerField("Sync Interval", default=1440)

    @staticmethod
    def get_type_name():
        return "dwd"

    def get_properties_json(self):
        return {
            "version": 1,
            "station_id": self.station_id,
        }
