from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi
from tsm.utils import encrypt_pw


class TSystemsApi(HttpApi):
    group = models.CharField("Group")
    station_id = models.CharField("Station ID")
    sync_interval = models.IntegerField("Sync Interval", default=60)
    username = models.CharField("Username")
    password = models.CharField("Password")

    @staticmethod
    def get_type_name():
        return "tsystems"

    def get_properties_json(self):
        return {
            "version": 1,
            "station_id": self.station_id,
            "group": self.group,
            "username": self.username,
            "password": encrypt_pw(self.password),
        }
