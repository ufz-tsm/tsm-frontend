from django.db import models

from tsm.models.Thing import Thing


class HttpApi(models.Model):
    thing = models.ForeignKey(Thing, on_delete=models.CASCADE)
    sync_enabled = models.BooleanField("Sync enabled", blank=True, null=True)
    sync_interval = models.IntegerField("Sync Interval", blank=True, null=True)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        # only save API of currently selected type
        if self.thing.http_api_type != self.get_type_name():
            return
        super(HttpApi, self).save()

    @staticmethod
    def get_type_name():
        return None

    def get_properties_json(self):
        return {"version": 1}

    def __str__(self):
        return str(self.id)

    class Meta:
        abstract = True
