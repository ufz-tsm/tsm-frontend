from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi
from tsm.utils import encrypt_pw


class TtnApi(HttpApi):
    api_key = models.CharField("API-Key", max_length=400, blank=True, null=True)
    endpoint_uri = models.CharField(
        "Endpoint-URI", max_length=400, blank=True, null=True
    )

    @staticmethod
    def get_type_name():
        return "ttn"

    def get_properties_json(self):
        return {
            "version": 1,
            "api_key": encrypt_pw(self.api_key),
            "endpoint_uri": self.endpoint_uri,
        }
