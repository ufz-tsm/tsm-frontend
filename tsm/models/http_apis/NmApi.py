from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi
from tsm.models.NmStation import NmStation


class NmApi(HttpApi):
    station = models.ForeignKey(
        NmStation, on_delete=models.CASCADE, null=True, blank=True
    )
    time_resolution = models.IntegerField(
        "Time resolution",
        blank=True,
        null=True,
        choices=[
            (0, "0 min"),
            (2, "2 min"),
            (5, "5 min"),
            (10, "10 min"),
            (30, "30 min"),
            (60, "1 h"),
            (120, "2 h"),
            (360, "6 h"),
            (720, "12 h"),
            (1440, "1 d"),
            (39276, "1 mo"),
            (525969, "1 y"),
        ],
        default=60,
    )

    @staticmethod
    def get_type_name():
        return "nm"

    def get_properties_json(self):
        return {
            "version": 1,
            "station_id": self.station.station_id if self.station is not None else None,
            "time_resolution": self.time_resolution,
        }
