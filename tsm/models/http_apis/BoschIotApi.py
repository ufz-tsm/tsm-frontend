from django.db import models

from tsm.models.http_apis.HttpApi import HttpApi
from tsm.utils import encrypt_pw


class BoschIotApi(HttpApi):
    username = models.CharField("Username", max_length=400, blank=True, null=True)
    password = models.CharField("Password", max_length=400, blank=True, null=True)
    sensor_id = models.CharField("Sensor-ID", max_length=400, blank=True, null=True)
    endpoint = models.CharField("Endpoint", max_length=400, blank=True, null=True)
    period = models.IntegerField("Period", blank=True, null=True)

    @staticmethod
    def get_type_name():
        return "bosch"

    def get_properties_json(self):
        return {
            "version": 1,
            "endpoint": self.endpoint,
            "username": self.username,
            "password": encrypt_pw(self.password),
            "sensor_id": self.sensor_id,
            "period": self.period,
        }
