from django.db import models

from tsm.models.QaqcTest import QaqcTest


class QaqcFunction(models.Model):
    qaqc_test = models.ForeignKey(QaqcTest, on_delete=models.CASCADE)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        # only save function of currently selected type
        if self.qaqc_test.qaqc_function_type != self.get_function_name():
            return
        super(QaqcFunction, self).save()

    @staticmethod
    def get_function_name():
        return None

    def get_properties_json(self):
        return {}

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = "QA/QC Function"
