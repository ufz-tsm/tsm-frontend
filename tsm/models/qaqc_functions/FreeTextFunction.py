from django.db import models

from tsm.models.qaqc_functions.QaqcFunction import QaqcFunction


class FreeTextFunction(QaqcFunction):
    function = models.CharField("Function", max_length=400, blank=True, null=True)

    @staticmethod
    def get_function_name():
        return "freetext"

    def get_properties_json(self):
        return {"function": self.function}
