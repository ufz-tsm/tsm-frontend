import uuid

from django.contrib.auth.models import Group
from django.db import models

from tsm.models.MqttDeviceType import MqttDeviceType
from tsm.models.Parser import Parser


class Thing(models.Model):
    name = models.CharField(max_length=1000)
    thing_id = models.UUIDField("ID", default=uuid.uuid4, editable=False)
    datasource_type = models.CharField(
        "Ingest Type",
        max_length=8,
        choices=[
            ("MQTT", "MQTT"),
            ("SFTP", "SFTP"),
            ("extSFTP", "External SFTP"),
            ("extAPI", "External API service"),
        ],
        default="SFTP",
    )
    group = models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name="Project")
    description = models.CharField(max_length=1000, blank=True, null=True)
    sftp_filename_pattern = models.CharField(
        "Filename pattern", max_length=200, blank=True, null=True
    )
    mqtt_uri = models.CharField("Broker URI", max_length=1000, blank=True, null=True)
    mqtt_username = models.CharField("Username", max_length=1000, blank=True, null=True)
    mqtt_password = models.CharField("Password", max_length=1000, blank=True, null=True)
    mqtt_hashed_password = models.CharField(max_length=256, blank=True, null=True)
    mqtt_topic = models.CharField("Topic", max_length=1000, blank=True, null=True)
    mqtt_device_type = models.ForeignKey(
        MqttDeviceType,
        verbose_name="Device Type",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    qaqc_ctx_window = models.CharField(
        "Context Window", max_length=100, blank=True, null=True, default=0
    )
    qaqc_tests = models.JSONField("Config Tests", blank=True, null=True)

    ext_sftp_uri = models.CharField(
        "Fileserver URI", max_length=200, blank=True, null=True
    )
    ext_sftp_path = models.CharField("Path", max_length=200, blank=True, null=True)
    ext_sftp_sync_enabled = models.BooleanField("Sync enabled", blank=True, null=True)
    ext_sftp_sync_interval = models.IntegerField("Sync Interval", blank=True, null=True)
    ext_sftp_username = models.CharField(
        "Username", max_length=200, blank=True, null=True
    )
    ext_sftp_password = models.CharField(
        "Password", max_length=200, blank=True, null=True
    )
    ext_sftp_public_key = models.CharField(
        "Public Key", max_length=1000, blank=True, null=True
    )

    parser = models.ManyToManyField(Parser)

    http_api_type = models.CharField(
        "External API Service",
        max_length=8,
        choices=[
            (None, "Select an external API service"),
            ("ttn", "The Things Network (TTN)"),
            ("dwd", "Deutscher Wetterdienst (DWD)"),
            ("nm", "Neutronmonitor (NM)"),
            ("bosch", "Bosch IoT"),
            ("tsystems", "TSystems"),
            ("uba", "Umweltbundesamt (UBA) Air Data"),
        ],
        blank=True,
        null=True,
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Thing"
        verbose_name_plural = "Things"
