from django.db import models

from tsm.models.QaqcSetting import QaqcSetting


class QaqcTest(models.Model):
    name = models.CharField("Name", max_length=1000)
    field = models.CharField("Field", max_length=1000)
    target = models.CharField("Target", max_length=1000)
    qaqc_function_type = models.CharField(
        "QA/QC Function",
        max_length=100,
        choices=[
            (None, "Select a function"),
            ("freetext", "Freetext"),
        ],
        blank=False,
        null=False,
    )
    qaqc_setting = models.ForeignKey(QaqcSetting, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name or self.id)

    class Meta:
        verbose_name = "QA/QC Test"
