from django.contrib.auth.models import Group
from django.db import models

from tsm.models.ParserType import ParserType


class Parser(models.Model):
    type = models.ForeignKey(ParserType, on_delete=models.CASCADE, null=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name="Project")
    name = models.CharField(max_length=1000, unique=True)

    def __str__(self):
        return str(self.name)

    def get_properties_json(self):
        return {}
