from django.db import models


class EncryptedCharField(models.CharField):
    def from_db_value(self, value, expression, connection):
        from tsm.utils import decrypt_pw

        if value is None:
            return value
        return decrypt_pw(value)

    def get_prep_value(self, value):
        from tsm.utils import encrypt_pw

        if value is None:
            return value
        return encrypt_pw(value)
