from django.db import models

from tsm.models.Parser import Parser


class CsvParser(Parser):
    delimiter = models.CharField(
        "Column delimiter", max_length=60, blank=True, null=True
    )
    exclude_headlines = models.PositiveIntegerField(
        "Number of headlines to exclude", default=0, blank=True, null=True
    )
    exclude_footlines = models.PositiveIntegerField(
        "Number of footlines to exclude", default=0, blank=True, null=True
    )
    pandas_read_csv = models.JSONField(blank=True, null=True)

    def __str__(self):
        return str(self.name)

    def get_properties_json(self):
        return {
            "delimiter": self.delimiter,
            "skipfooter": self.exclude_footlines,
            "skiprows": self.exclude_headlines,
            "pandas_read_csv": self.pandas_read_csv,
            "timestamp_columns": [
                {"column": ts.column, "format": ts.format}
                for ts in self.timestamp_columns.all()
            ],
        }
