from django.db import models

from tsm.models.Thing import Thing


class RawDataStorage(models.Model):
    bucket = models.CharField(max_length=63)
    access_key = models.CharField(max_length=200)
    secret_key = models.CharField(max_length=200)
    fileserver_uri = models.URLField("Fileserver URI", blank=True, null=True)
    thing = models.OneToOneField(
        Thing,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    def __str__(self):
        return str(self.bucket)
