from django.contrib.auth.models import Group
from django.db import models
from django.db.models.constraints import UniqueConstraint, Q


class QaqcSetting(models.Model):
    name = models.CharField(max_length=1000, unique=True)
    context_window = models.CharField(max_length=1000, verbose_name="Context window")
    group = models.ForeignKey(Group, on_delete=models.CASCADE, verbose_name="Project")
    active = models.BooleanField(default=False, verbose_name="Active")

    def __str__(self):
        return str(self.name)

    def clean(self):
        if self.active and self.group:
            existing_active = QaqcSetting.objects.filter(
                group=self.group, active=True
            ).exclude(pk=self.pk)

            if existing_active.exists():
                existing_active.update(active=False)
        super().clean()

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "QA/QC Setting"
        constraints = [
            UniqueConstraint(
                fields=["active", "group"],
                condition=Q(active=True),
                name="one_active_setting_per_group",
            )
        ]
