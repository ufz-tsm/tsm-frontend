from django.db import models

from tsm.models.CsvParser import CsvParser


class TimestampColumn(models.Model):
    """New model to store timestamp columns separately"""

    csv_parser = models.ForeignKey(
        CsvParser, on_delete=models.CASCADE, related_name="timestamp_columns"
    )
    column = models.PositiveIntegerField()
    format = models.CharField(max_length=200)

    def __str__(self):
        return f"Column {self.column} - {self.format}"
