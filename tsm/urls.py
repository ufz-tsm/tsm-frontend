from django.urls import path

from . import views

app_name = "tsm"

urlpatterns = [
    path("about/", views.about, name="about"),
    path("get_parser_objects/", views.get_parser_objects, name="get_parser_objects"),
    path(
        "get_things_and_datastreams_from_sta/",
        views.get_things_and_datastreams_from_sta,
        name="get_things_and_datastreams_from_sta",
    ),
]
