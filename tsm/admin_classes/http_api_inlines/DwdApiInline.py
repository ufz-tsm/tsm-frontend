from django.contrib import admin
from tsm.models import DwdApi
from tsm.form_classes.http_api_forms.dwdApiForm import DwdApiForm
from tsm.utils import get_time_with_unit_by_time_in_minutes


class DwdApiInline(admin.StackedInline):
    model = DwdApi
    classes = ("http-api-settings", "http-api-dwd")
    form = DwdApiForm
    can_delete = False
    verbose_name = "Deutscher Wetterdienst API Settings"

    fieldsets = [
        (
            None,
            {
                "description": """
                    For more information on Deutscher Wetterdienst API properties, visit the API documentation 
                    <b><a href='https://brightsky.dev/docs/#/operations/getWeather'>here</a></b>.
                    """,
                "fields": (
                    "sync_enabled",
                    "sync_interval_string",
                    "station_id",
                ),
            },
        ),
    ]
    max_num = 1

    def get_readonly_fields(self, request, obj=None):
        return ["sync_interval_string"]

    def sync_interval_string(self, obj):
        value, unit = get_time_with_unit_by_time_in_minutes(obj.sync_interval)
        return f"{value} {unit}"

    sync_interval_string.short_description = "Sync-Interval"
