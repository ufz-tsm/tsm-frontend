from django.contrib import admin

from tsm.form_classes.http_api_forms.tSystemsApiForm import TSystemsApiForm
from tsm.models import TSystemsApi
from tsm.utils import get_time_with_unit_by_time_in_minutes


class TSystemsApiInline(admin.StackedInline):
    model = TSystemsApi
    classes = ("http-api-settings", "http-api-tsystems")
    form = TSystemsApiForm
    can_delete = False
    verbose_name = "TSystems API Settings"
    fieldsets = [
        (
            None,
            {
                "description": """
                    For more information on TSystems API properties, visit the API documentation 
                    <b><a href='https://sensorstation.caritc.de/sensorstation-management/swagger-ui/index.html'>here</a></b>.
                    """,
                "fields": (
                    "sync_enabled",
                    "sync_interval_string",
                    "station_id",
                    "group",
                ),
            },
        ),
        (
            "TSystems API Authentication",
            {
                "fields": (
                    "username",
                    "password",
                )
            },
        ),
    ]
    max_num = 1

    def get_readonly_fields(self, request, obj=None):
        return ["sync_interval_string"]

    def sync_interval_string(self, obj):
        value, unit = get_time_with_unit_by_time_in_minutes(obj.sync_interval)
        return f"{value} {unit}"

    sync_interval_string.short_description = "Sync-Interval"
