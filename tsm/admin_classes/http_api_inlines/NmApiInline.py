from django.contrib import admin
from tsm.models import NmApi
from tsm.form_classes.http_api_forms.nmApiForm import NmApiForm


class NmApiInline(admin.StackedInline):
    model = NmApi
    classes = ("http-api-settings", "http-api-nm")
    form = NmApiForm
    can_delete = False
    verbose_name = "Neutronmonitor API Settings"
    autocomplete_fields = ["station"]
    fieldsets = [
        (
            None,
            {
                "description": """
                    For more information on Neutronmonitor API properties, visit the API documentation 
                    <b><a href='https://www.nmdb.eu/nest/help.php#howto'>here</a></b>.
                    """,
                "fields": (
                    "sync_enabled",
                    "sync_interval",
                    "time_resolution",
                    "station",
                ),
            },
        ),
    ]
    max_num = 1
