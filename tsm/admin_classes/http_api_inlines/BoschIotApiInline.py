from django.contrib import admin
from tsm.models import BoschIotApi
from tsm.form_classes.http_api_forms.boschIotApiForm import BoschIotApiForm


class BoschIotApiInline(admin.StackedInline):
    model = BoschIotApi
    classes = ("http-api-settings", "http-api-bosch")
    form = BoschIotApiForm
    can_delete = False
    verbose_name = "Bosch IoT API Settings"
    fieldsets = [
        (
            None,
            {
                "description": None,
                "fields": (
                    "sync_enabled",
                    "sync_interval",
                    "endpoint",
                    "username",
                    "password",
                    "sensor_id",
                    "period",
                ),
            },
        ),
    ]
    max_num = 1
