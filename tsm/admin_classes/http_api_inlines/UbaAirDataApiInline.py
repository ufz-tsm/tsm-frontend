from django.contrib import admin

from tsm.form_classes.http_api_forms.ubaAirDataApiForm import UbaAirDataApiForm
from tsm.models import UbaAirDataApi
from tsm.utils import get_time_with_unit_by_time_in_minutes


class UbaAirDataApiInline(admin.StackedInline):
    model = UbaAirDataApi
    classes = ("http-api-settings", "http-api-uba")
    form = UbaAirDataApiForm
    can_delete = False
    verbose_name = "Umweltbundesamt (UBA) Air Data API Settings"
    fieldsets = [
        (
            None,
            {
                "description": """
                    For more information on UBA Air Data API properties, visit the API documentation 
                    <b><a href='https://luftqualitaet.api.bund.dev/'>here</a></b>.
                    """,
                "fields": (
                    "sync_enabled",
                    "sync_interval_string",
                    "station_id",
                ),
            },
        ),
    ]
    max_num = 1

    def get_readonly_fields(self, request, obj=None):
        return ["sync_interval_string"]

    def sync_interval_string(self, obj):
        value, unit = get_time_with_unit_by_time_in_minutes(obj.sync_interval)
        return f"{value} {unit}"

    sync_interval_string.short_description = "Sync-Interval"
