from django.contrib import admin
from tsm.models import TtnApi
from tsm.form_classes.http_api_forms.ttnApiForm import TtnApiForm


class TtnApiInline(admin.StackedInline):
    model = TtnApi
    classes = ("http-api-settings", "http-api-ttn")
    form = TtnApiForm
    can_delete = False
    verbose_name = "The Things Network API Settings"
    fieldsets = [
        (
            None,
            {
                "description": """
                    For more information on The Things Network API properties, visit the API documentation 
                    <b><a href='https://www.thethingsindustries.com/docs/api/'>here</a></b>.
                    """,
                "fields": ("sync_enabled", "sync_interval", "endpoint_uri", "api_key"),
            },
        ),
    ]
    max_num = 1
