from django.contrib import admin
from django.contrib.auth.models import Group
from django.utils.html import format_html
from django.urls import path
from django.shortcuts import render, redirect
from django.contrib import messages

from tsm.admin_classes.http_api_inlines.TSystemsApiInline import TSystemsApiInline
from tsm.admin_classes.http_api_inlines.UbaAirDataApiInline import UbaAirDataApiInline
from tsm.admin_classes.projectFilter import ProjectFilter
from tsm.admin_classes.http_api_inlines.DwdApiInline import DwdApiInline
from tsm.admin_classes.http_api_inlines.TtnApiInline import TtnApiInline
from tsm.admin_classes.http_api_inlines.NmApiInline import NmApiInline
from tsm.admin_classes.http_api_inlines.BoschIotApiInline import BoschIotApiInline
from tsm.form_classes import TSystemsHistoricDataForm

from tsm.form_classes.groupChoiceField import GroupChoiceField
from tsm.form_classes.thingForm import ThingForm
from tsm.form_classes.thingFormEditParserWasNotSelected import (
    ThingFormEditParserWasNotSelected,
)
from tsm.form_classes.thingFormEditParserWasSelected import (
    ThingFormEditParserWasSelected,
)
from tsm.models import Parser, Thing
from tsm.models import TSystemsApi
from tsm.utils import (
    get_group_name_with_vo_prefix,
    get_connection_string,
    get_sftp_uri,
    get_sftp_username,
    get_sftp_password,
    get_mqtt_username,
    get_mqtt_password,
    publish_tsystems_historic_data,
)


class ThingAdmin(admin.ModelAdmin):

    def _republish_thing(self, modeladmin, request, queryset):
        import tsm.signals

        for thing in queryset:
            tsm.signals.publish_thing(thing)

    _republish_thing.name = "republish_thing"
    _republish_thing.description = "Re-publish selected things via mqtt"

    def get_actions(self, request):
        actions = super(ThingAdmin, self).get_actions(request)
        func = self._republish_thing
        if request.user.is_superuser:
            actions[func.name] = func, func.name, func.description  # noqa
        return actions

    def custom_group(self, obj):
        # Access the related group object and return its custom display
        return get_group_name_with_vo_prefix(obj.group)

    def external_api_action(self, obj: Thing):
        if obj.http_api_type == TSystemsApi.get_type_name():
            return format_html(
                '<a class="button" href="{}">Sync historic T-Systems data</a>',
                f"{obj.id}/tsystems-historic-data",
            )
        return format_html("")

    def get_form(self, request, obj=None, **kwargs):
        # Use the edit form when editing an existing instance

        if obj:
            kwargs["form"] = ThingFormEditParserWasSelected
            first_parser = obj.parser.first()
            if first_parser:
                form = super().get_form(request, obj, **kwargs)
                form.base_fields["parser"].queryset = Parser.objects.filter(
                    type_id=first_parser.type.id, group=obj.group
                )
            else:
                kwargs["form"] = ThingFormEditParserWasNotSelected
        return super().get_form(request, obj, **kwargs)

    custom_group.short_description = "Project"

    inlines = [
        DwdApiInline,
        TtnApiInline,
        NmApiInline,
        BoschIotApiInline,
        UbaAirDataApiInline,
        TSystemsApiInline,
    ]

    fieldsets = [
        (None, {
            'fields': (
                'name', 'group', 'description', 'thing_id', get_connection_string, 'datasource_type'),
        }),
        ('SFTP Settings', {
            'fields': (
                'sftp_filename_pattern', 'parser_type', 'parser'),
            'classes': ('sftp-settings', 'ext-sftp-settings'),
        }),
        ('SFTP-Server Settings', {
            'fields': (get_sftp_uri, (get_sftp_username, get_sftp_password),),
            'classes': ('sftp-settings',),
        }),
        ('External SFTP-Server Settings', {
            'fields': (
                'ext_sftp_sync_enabled',
                'ext_sftp_uri',
                'ext_sftp_path',
                ('ext_sftp_username', 'ext_sftp_password'),
                'ext_sftp_sync_interval',
                'ext_sftp_public_key',),
            'classes': ('ext-sftp-settings',),
        }),
        ('MQTT Settings', {
            'fields': (('mqtt_uri', 'mqtt_topic'), (get_mqtt_username, get_mqtt_password), 'mqtt_device_type',),
            'classes': ('mqtt-settings',),
        }),
        ('External API Service', {
            'fields': ('http_api_type',),
            'classes': ('http-api-settings',),
        }),
        ('Qa/Qc Settings', {
            'fields': ('qaqc_ctx_window', 'qaqc_tests',),
            'classes': ('mqtt-settings', 'sftp-settings', 'ext-sftp-settings', 'http-api-settings'),
        }),
    ]  # fmt: skip

    form = ThingForm

    list_display = (
        "name",
        "thing_id",
        "custom_group",
        "datasource_type",
        "external_api_action",
    )
    list_filter = (
        "datasource_type",
        ProjectFilter,
    )
    get_connection_string.short_description = "DB-Connection"
    get_sftp_uri.short_description = "Fileserver URI"
    get_sftp_username.short_description = "Username"
    get_sftp_password.short_description = "Password"
    get_mqtt_username.short_description = "Username"
    get_mqtt_password.short_description = "Password"
    external_api_action.short_description = "External Api Action"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # if not superuser, a user can only select things from his groups (=projects)
        if db_field.name == "group":
            if not request.user.is_superuser:
                kwargs["queryset"] = Group.objects.filter(user=request.user)
            if "add" in request.path:
                kwargs["form_class"] = GroupChoiceField

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        fields = [
            "thing_id",
            get_connection_string,
            "is_created",
            "ext_sftp_public_key",
            get_sftp_uri,
            get_sftp_username,
            get_sftp_password,
            get_mqtt_username,
            get_mqtt_password,
        ]
        return fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # if not superuser, a user can see only things from his groups (=projects)
        users_groups = request.user.groups.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=users_groups)

    def save_model(self, request, obj: Thing, form, change):
        if obj.datasource_type in ["SFTP", "extSFTP"] and "parser" in form.changed_data:
            obj.m2m_will_change = True
        super().save_model(request, obj, form, change)
        pass

    def tsystems_historic_data_form_view(self, request, thing_id):
        if request.method == "POST":
            form = TSystemsHistoricDataForm(request.POST)
            if form.is_valid():

                thing = Thing.objects.get(id=thing_id)

                publish_tsystems_historic_data(
                    thing.thing_id,
                    form.cleaned_data["begin_date"],
                    form.cleaned_data["end_date"],
                )

                messages.success(
                    request,
                    f"Successfully published",
                )
                return redirect("..")

        else:
            form = TSystemsHistoricDataForm()
        return render(request, "admin/tsystems_historic_data_form.html", {"form": form})

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "<thing_id>/tsystems-historic-data",
                self.admin_site.admin_view(self.tsystems_historic_data_form_view),
                name="tsystem-historic-data-form",
            ),
        ]
        return custom_urls + urls

    class Media:
        js = ("thing_form.js",)
