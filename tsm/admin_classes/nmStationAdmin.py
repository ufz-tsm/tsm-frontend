from django.contrib import admin

from tsm.form_classes.nmStationForm import NmStationForm


class NmStationAdmin(admin.ModelAdmin):

    search_fields = ["station_id"]

    form = NmStationForm

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def get_model_perms(self, request):
        if not request.user.is_superuser:
            return {}
        return super(NmStationAdmin, self).get_model_perms(request)
