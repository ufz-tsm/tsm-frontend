import nested_admin.nested
from django.contrib.auth.models import Group

from tsm.admin_classes.projectFilter import ProjectFilter
from django.shortcuts import render, redirect
from django.urls import path
from django.contrib import messages
from django.utils.html import format_html

from tsm.admin_classes.qaqcTestInline import QaqcTestInline
from tsm.form_classes.groupChoiceField import GroupChoiceField
from tsm.form_classes.qaqcSettingForm import QaqcSettingForm
from tsm.form_classes.qaqcSettingRunForm import QaqcSettingRunForm
from tsm.utils import (
    publish_qaqc_setting_data_parsed,
)
from tsm.models import QaqcSetting, QaqcTest
from tsm.utils import get_group_name_with_vo_prefix


class QaqcSettingAdmin(nested_admin.nested.NestedModelAdmin):
    form = QaqcSettingForm

    list_display = [
        "name",
        "test_amount",
        "custom_group",
        "is_active_dot",
        "run_setting_button",
    ]
    actions = ["open_run_setting_form"]

    inlines = [QaqcTestInline]
    list_filter = (
        "active",
        ProjectFilter,
    )

    def custom_group(self, obj):
        # Access the related group object and return its custom display
        return get_group_name_with_vo_prefix(obj.group)

    custom_group.short_description = "Project"

    def test_amount(self, obj: QaqcSetting):
        return QaqcTest.objects.filter(qaqc_setting=obj).count()

    test_amount.short_description = "# QA/QC-Tests"

    def is_active_dot(self, obj):
        if obj.active:
            return format_html(
                f'<div style="height: 1em; width: 1em; border-radius: 1em; background-color: green; border: 2px solid green;"></div>'
            )
        return format_html(
            f'<div style="height: 1em; width: 1em; border-radius: 2em; border: 2px solid grey;"></div>'
        )

    is_active_dot.short_description = "Active"

    def open_run_setting_form(self, request, queryset):
        selected = ",".join(str(obj.pk) for obj in queryset)
        return redirect(f"run-qaqc-form/?ids={selected}")

    open_run_setting_form.short_description = "Run selected QA/QC settings"

    def run_setting_button(self, obj):
        return format_html(
            f'<a href="run-qaqc-form/?ids={obj.pk}" class="button">Run Setting</a>'
        )

    run_setting_button.allow_tags = True
    run_setting_button.short_description = "Run QA/QC setting"

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "run-qaqc-form/",
                self.admin_site.admin_view(self.run_qaqc_setting_form_view),
                name="run-qaqc-form",
            ),
        ]
        return custom_urls + urls

    def run_qaqc_setting_form_view(self, request):
        if request.method == "POST":
            form = QaqcSettingRunForm(request.POST)
            if form.is_valid():
                qaqc_setting_ids = request.GET.get("ids").split(",")
                qaqc_settings = QaqcSetting.objects.filter(id__in=qaqc_setting_ids)
                for qaqc_setting in qaqc_settings:
                    publish_qaqc_setting_data_parsed(
                        qaqc_setting,
                        form.cleaned_data["begin_date"],
                        form.cleaned_data["end_date"],
                    )

                messages.success(
                    request,
                    f"Successfully published {', '.join([q.name for q in qaqc_settings])}",
                )
                return redirect("..")
        else:
            form = QaqcSettingRunForm()

        return render(request, "admin/run_qaqc_setting_form.html", {"form": form})

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # if not superuser, a user can only select things from his groups (=projects)
        if db_field.name == "group":
            if not request.user.is_superuser:
                kwargs["queryset"] = Group.objects.filter(user=request.user)
                kwargs["form_class"] = GroupChoiceField

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # if not superuser, a user can see only things from his groups (=projects)
        users_groups = request.user.groups.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=users_groups)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            super().save_model(request, obj, form, change)
        else:
            pass

    def save_related(self, request, form, formsets, change):
        form.save_m2m()
        for formset in formsets:
            self.save_formset(request, form, formset, change=change)
        super().save_model(request, form.instance, form, change)

    fieldsets = [
        (None, {
            'fields': (
                'active', 'name', 'group', 'context_window'),
        }),
    ]  # fmt: skip
