import nested_admin.nested

from tsm.models.qaqc_functions.QaqcFunction import QaqcFunction


class QaqcFunctionInline(nested_admin.nested.NestedStackedInline):
    model = QaqcFunction
    can_delete = True
    classes = ["qaqc-test-function"]
    max_num = 1
    min_num = 1
