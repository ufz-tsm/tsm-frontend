import nested_admin.nested

from tsm.admin_classes import FreeTextFunctionInline
from tsm.form_classes.qaqcTestForm import QaqcTestForm
from tsm.models import QaqcTest


class QaqcTestInline(nested_admin.nested.NestedStackedInline):
    model = QaqcTest
    classes = ["qaqc-test"]
    form = QaqcTestForm
    inlines = [FreeTextFunctionInline]
    can_delete = True
    verbose_name = "QA/QC Test"
    extra = 0
    min_num = 1

    class Media:
        js = ("qaqc_test_form.js",)
        css = {"all": ("qaqc_test_form.css",)}
