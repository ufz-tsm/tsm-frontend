from django.contrib import admin
from django.contrib.auth.models import Group

from tsm.utils import filter_group_queryset_in_allowed_vo, get_group_name_with_vo_prefix


class ProjectFilter(admin.SimpleListFilter):
    title = "Projects"
    parameter_name = "project"

    def lookups(self, request, model_admin):
        result = []
        if request.user.is_superuser:
            queryset = Group.objects.all()
            groups = filter_group_queryset_in_allowed_vo(queryset)
        else:
            queryset = request.user.groups.all()
            groups = filter_group_queryset_in_allowed_vo(queryset)
            groups = groups.filter(name__contains="#")
            groups = groups.filter(user__isnull=False).distinct()
            groups = groups.filter(name__regex=r"^[^:]+(:[^:]+){5}$")

        for group in groups:
            group_name = get_group_name_with_vo_prefix(group)
            result.append((group.id, group_name))
        return result

    def queryset(self, request, query_set):
        if self.value():
            return query_set.filter(group__id=self.value())
        else:
            return query_set
