from django.contrib import admin
from django.contrib.auth.models import Group

from tsm.form_classes.csvParserFormAdmin import CsvParserFormAdmin
from tsm.form_classes.csvParserFormUser import CsvParserFormUser
from tsm.form_classes.groupChoiceField import GroupChoiceField
from tsm.form_classes.timestampColumnInline import TimestampColumnInline
from tsm.utils import get_group_name_with_vo_prefix


class CsvParserAdmin(admin.ModelAdmin):
    # exclude = ['pandas_read_csv']
    inlines = [TimestampColumnInline]

    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            kwargs["form"] = CsvParserFormAdmin
        else:
            kwargs["form"] = CsvParserFormUser
        return super().get_form(request, obj, **kwargs)

    def custom_group(self, obj):
        # Access the related group object and return its custom display
        return get_group_name_with_vo_prefix(obj.group)

    custom_group.short_description = "Project"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # if not superuser, a user can only select things from his groups (=projects)
        if db_field.name == "group":
            if not request.user.is_superuser:
                kwargs["queryset"] = Group.objects.filter(user=request.user)
            kwargs["form_class"] = GroupChoiceField

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # if not superuser, a user can see only csvparser from his groups (=projects)
        users_groups = request.user.groups.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=users_groups)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj=obj)

        # Check if the value of "pandas_read_csv" is not null for the current model object and the user is no superuser
        if obj and obj.pandas_read_csv is not None and not request.user.is_superuser:
            readonly_fields += ("pandas_read_csv",)

        return readonly_fields
