from tsm.admin_classes.qaqcFunctionInline import QaqcFunctionInline
from tsm.form_classes.qaqc_function_forms.freeTextFunctionForm import (
    FreeTextFunctionForm,
)
from tsm.models.qaqc_functions.FreeTextFunction import FreeTextFunction


class FreeTextFunctionInline(QaqcFunctionInline):
    model = FreeTextFunction
    classes = ["qaqc-test-function", "qaqc-test-function-freetext"]
    form = FreeTextFunctionForm
