from django import forms

from tsm.form_classes.thingDatastreamTupleSelectorField import (
    ThingDatastreamTupleSelectorField,
)
from tsm.models import QaqcTest


class QaqcTestForm(forms.ModelForm):
    field = ThingDatastreamTupleSelectorField(
        label="Field",
        help_text="'Ctrl' + Click to select multiple.",
    )
    target = ThingDatastreamTupleSelectorField(
        label="Target",
        help_text="'Ctrl' + Click to select multiple.",
    )

    # fields become hidden by JS
    current_field = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={"readonly": "readonly"}),
    )
    current_target = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={"readonly": "readonly"}),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.fields["current_field"].initial = str(
                getattr(self.instance, "field", "")
            )
            self.fields["current_target"].initial = str(
                getattr(self.instance, "target", "")
            )

    def clean(self):
        cleaned_data = super().clean()

    class Meta:
        model = QaqcTest
        fields = "__all__"
