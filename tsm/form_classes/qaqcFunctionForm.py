from django import forms

from tsm.models.qaqc_functions.QaqcFunction import QaqcFunction


class QaqcFunctionForm(forms.ModelForm):
    def get_function_model(self):
        return self.Meta.model

    def get_required_fields(self):
        return []

    def clean(self):
        cleaned_data = super().clean()
        # only validate form if function type is the selected one
        if "qaqc_test" in cleaned_data:
            if (
                cleaned_data["qaqc_test"].qaqc_function_type
                != self.get_function_model().get_function_name()
            ):
                return

        # validate required fields
        for field in self.get_required_fields():
            if cleaned_data.get(field):
                continue
            self.add_error(field, "This field could not be empty.")

        self.custom_clean(cleaned_data)

    def custom_clean(self, cleaned_data):
        return

    class Meta:
        model = QaqcFunction
        fields = "__all__"
