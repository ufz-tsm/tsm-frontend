from django import forms

from tsm.form_classes.parserChoiceField import ParserChoiceField
from tsm.form_classes.syncIntervalChoiceField import SyncIntervalChoiceField
from tsm.models import ParserType, Thing
from tsm.utils import custom_clean


class ThingForm(forms.ModelForm):
    description = forms.CharField(
        widget=forms.Textarea(attrs={"cols": 60, "rows": 2}), required=False
    )
    parser_type = forms.ModelChoiceField(
        queryset=ParserType.objects.all(), label="Select parser type", required=False
    )
    parser = ParserChoiceField(label="Select parser")
    ext_sftp_sync_enabled = forms.BooleanField(
        label="Enable fileserver sync", initial=True, required=False
    )
    ext_sftp_sync_interval = SyncIntervalChoiceField(required=False)

    class Meta:
        model = Thing
        fields = "__all__"

    def clean(self):
        cleaned_data = super().clean()
        custom_clean(self, cleaned_data)
