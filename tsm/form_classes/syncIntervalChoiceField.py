from django import forms

from tsm.form_classes.syncIntervalChoiceWidget import SyncIntervalChoiceWidget


class SyncIntervalChoiceField(forms.MultiValueField):
    widget = SyncIntervalChoiceWidget

    UNIT_CHOICES = (
        ("min", "Minutes"),
        ("h", "Hours"),
        ("d", "Days"),
    )

    max_value = 3600

    def __init__(self, label="Sync-Interval", **kwargs):
        fields = (
            forms.IntegerField(min_value=0),
            forms.ChoiceField(choices=self.UNIT_CHOICES),
        )
        super().__init__(fields=fields, **kwargs)
        self.label = label

    def compress(self, data_list):
        if data_list:
            value, unit = data_list
            if unit == "min":
                return value
            elif unit == "h":
                return value * 60
            elif unit == "d":
                return value * 60 * 24
        return None

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if isinstance(widget, SyncIntervalChoiceWidget):
            # set default value
            attrs.update({"value": "15"})
        return attrs
