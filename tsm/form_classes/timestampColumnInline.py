from django.contrib import admin

from tsm.form_classes.timestampColumnForm import TimestampColumnFormSet
from tsm.models import TimestampColumn


class TimestampColumnInline(admin.TabularInline):  # or admin.StackedInline
    """Inline admin to edit timestamp columns directly within CsvParser"""

    model = TimestampColumn
    formset = TimestampColumnFormSet
    extra = 1  # Show an empty row for adding a new entry
