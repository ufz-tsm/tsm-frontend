from django import forms

from tsm.models import NmStation, QaqcSetting


class QaqcSettingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.pk:
            return

        self.fields["group"].help_text = (
            "Changing the project will remove all fields and targets of the present QA/QC tests."
        )

        current_setting = self.instance
        group = current_setting.group
        active_setting = QaqcSetting.objects.filter(group=group, active=True).first()
        if active_setting and active_setting.id != current_setting.id:
            self.fields["active"].help_text = (
                f"Currently active: <b>{active_setting}</b>"
            )

    class Meta:
        model = NmStation
        fields = "__all__"
