from django import forms
from django.contrib.auth.models import Group


class GroupHiddenInput(forms.HiddenInput):
    error_messages = {
        "error": "Erroc Code 001. Please contact an admin.",
    }

    def clean(self, value):
        return value

    def to_python(self, value):
        # Convert the submitted value to the Python representation
        try:
            # Perform any necessary parsing or data conversion here
            group = Group.objects.get(pk=value)
            return group
        except (ValueError, Group.DoesNotExist):
            raise forms.ValidationError("Invalid group value")

    def has_changed(self, initial, data):
        data = self.to_python(data)
        return initial != data
