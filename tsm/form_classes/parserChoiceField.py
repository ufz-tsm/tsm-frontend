from django import forms
from django.forms import Select

from tsm.models import Parser


class ParserChoiceField(forms.Field):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Customize your field attributes here if needed
        self.widget = Select()

    def to_python(self, value):
        # Convert the submitted value to the Python representation
        if value in self.empty_values:
            return []
        try:
            # Perform any necessary parsing or data conversion here
            parser = Parser.objects.get(pk=value)  # Example: Retrieve a Parser object
            # return an array becaus we have a many to many relationship to thing
            return [parser]
        except (ValueError, Parser.DoesNotExist):
            raise forms.ValidationError("Invalid parser value")

    def validate(self, value):
        # Perform additional validation here if needed
        pass
