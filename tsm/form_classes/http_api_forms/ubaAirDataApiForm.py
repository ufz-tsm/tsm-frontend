from django import forms

from tsm.models import UbaAirDataApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm


class UbaAirDataApiForm(HttpApiForm):
    station_id = forms.CharField(
        widget=forms.TextInput,
        label="Station ID",
        required=False,
    )

    def get_required_fields(self):
        return ["station_id"]

    class Meta:
        model = UbaAirDataApi
        fields = "__all__"
