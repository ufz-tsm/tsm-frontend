from django import forms

from tsm.models.http_apis.DwdApi import DwdApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm


class DwdApiForm(HttpApiForm):
    station_id = forms.CharField(
        widget=forms.TextInput,
        label="Station ID",
        help_text="ID of the DWD weather station (usually 5 numeric characters)",
        required=False,
    )

    def get_required_fields(self):
        return ["station_id"]

    def custom_clean(self, cleaned_data):
        if "station_id" in cleaned_data and len(str(cleaned_data["station_id"])) != 5:
            self.add_error("station_id", "Ensure this value has exactly 5 characters.")

    class Meta:
        model = DwdApi
        fields = "__all__"
