from tsm.models.http_apis.NmApi import NmApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm


class NmApiForm(HttpApiForm):

    def get_required_fields(self):
        return ["station", "time_resolution"]

    class Meta:
        model = NmApi
        fields = "__all__"
        labels = {
            "station": "Station ID",
        }
