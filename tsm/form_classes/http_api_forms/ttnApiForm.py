from tsm.models.http_apis.TtnApi import TtnApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm


class TtnApiForm(HttpApiForm):

    def get_required_fields(self):
        return ["api_key", "endpoint_uri"]

    class Meta:
        model = TtnApi
        fields = "__all__"
