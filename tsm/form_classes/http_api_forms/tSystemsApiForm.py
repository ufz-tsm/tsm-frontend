from django import forms

from tsm.models import TSystemsApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm


class TSystemsApiForm(HttpApiForm):
    station_id = forms.CharField(
        widget=forms.TextInput,
        label="Station ID",
        required=False,
    )
    group = forms.CharField(
        widget=forms.TextInput,
        label="Group",
        required=False,
    )
    username = forms.CharField(
        help_text="Username for authentication.",
        widget=forms.TextInput,
        label="Username",
        required=False,
    )
    password = forms.CharField(
        help_text="Password for authentication.",
        widget=forms.TextInput,
        label="Password",
        required=False,
    )

    def get_required_fields(self):
        return [
            "station_id",
            "group",
            "username",
            "password",
        ]

    class Meta:
        model = TSystemsApi
        fields = "__all__"
