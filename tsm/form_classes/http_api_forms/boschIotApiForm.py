from django import forms

from tsm.models.http_apis.BoschIotApi import BoschIotApi
from tsm.form_classes.http_api_forms.httpApiForm import HttpApiForm
from tsm.form_classes.syncIntervalChoiceField import SyncIntervalChoiceField


class BoschIotApiForm(HttpApiForm):
    endpoint = forms.CharField(
        widget=forms.TextInput,
        label="Endpoint",
        required=False,
    )
    sensor_id = forms.CharField(
        widget=forms.TextInput,
        label="Sensor ID",
        required=False,
    )
    username = forms.CharField(
        widget=forms.TextInput,
        label="Username",
        required=False,
    )
    password = forms.CharField(
        widget=forms.TextInput,
        label="Password",
        required=False,
    )
    period = SyncIntervalChoiceField(
        required=False,
        label="Period",
        help_text="The data is retrieved within the specified period before the current time.",
    )

    def get_required_fields(self):
        return ["username", "password", "endpoint", "sensor_id", "period"]

    class Meta:
        model = BoschIotApi
        fields = "__all__"
