from django import forms

from tsm.form_classes.syncIntervalChoiceField import SyncIntervalChoiceField


class HttpApiForm(forms.ModelForm):
    sync_enabled = forms.BooleanField(
        label="Enable API sync", initial=True, required=False
    )
    sync_interval = SyncIntervalChoiceField(required=False)

    def get_api_model(self):
        return self.Meta.model

    def get_required_fields(self):
        return []

    def clean(self):
        cleaned_data = super().clean()
        # only validate form if API type is the selected one
        if "thing" in cleaned_data:
            if (
                cleaned_data["thing"].http_api_type
                != self.get_api_model().get_type_name()
                or cleaned_data["thing"].datasource_type != "extAPI"
            ):
                return

        # validate required fields
        for field in self.get_required_fields():
            if cleaned_data.get(field):
                continue
            self.add_error(field, "This field could not be empty.")

        self.custom_clean(cleaned_data)

    def custom_clean(self, cleaned_data):
        return

    class Meta:
        abstract = True
