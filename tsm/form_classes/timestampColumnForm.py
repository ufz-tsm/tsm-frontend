from django import forms
from django.forms import inlineformset_factory
from tsm.models import CsvParser, TimestampColumn


class TimestampColumnForm(forms.ModelForm):
    """Form for timestamp columns with validation"""

    class Meta:
        model = TimestampColumn
        fields = ["column", "format"]

    def clean(self):
        """Custom validation to ensure valid columns and formats"""
        cleaned_data = super().clean()
        column = cleaned_data.get("column")
        format_str = cleaned_data.get("format")

        if column is None or column < 0:
            raise forms.ValidationError(
                {"column": "Column index must be a positive number."}
            )

        if not format_str or not format_str.strip():
            raise forms.ValidationError({"format": "Format cannot be empty."})

        return cleaned_data


TimestampColumnFormSet = inlineformset_factory(
    CsvParser,
    TimestampColumn,
    form=TimestampColumnForm,
    extra=1,  # Allows adding new entries
    can_delete=True,  # Allows deleting timestamp columns
)
