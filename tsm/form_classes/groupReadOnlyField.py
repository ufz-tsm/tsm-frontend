from django import forms

from tsm.form_classes.groupHiddenInput import GroupHiddenInput
from tsm.form_classes.groupReadOnlyWidget import GroupReadOnlyWidget


class GroupReadOnlyField(forms.MultiValueField):
    widget = GroupReadOnlyWidget

    def __init__(self, **kwargs):
        fields = (forms.CharField(), GroupHiddenInput())
        super().__init__(fields=fields, **kwargs)

    def compress(self, data_list):
        group_name, group = data_list
        return group
