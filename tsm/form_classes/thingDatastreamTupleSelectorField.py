from django import forms

from tsm.form_classes.thingDatastreamTupleSelectorWidget import (
    ThingDatastreamTupleSelectorWidget,
)


class ThingDatastreamTupleSelectorField(forms.MultiValueField):
    widget = ThingDatastreamTupleSelectorWidget

    def __init__(self, **kwargs):
        fields = (
            forms.CharField(),
            forms.CharField(),
        )
        super().__init__(fields=fields, **kwargs)

    def compress(self, data_list):
        return data_list[1] if data_list else None
