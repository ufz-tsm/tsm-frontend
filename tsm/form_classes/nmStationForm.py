from django import forms

from tsm.models import NmStation


class NmStationForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        if "station_id" in cleaned_data:
            if (
                len(
                    NmStation.objects.filter(
                        station_id=str(cleaned_data["station_id"]).upper()
                    )
                )
                != 0
            ):
                self.add_error("station_id", "This Station ID already exists.")
        pass

    class Meta:
        model = NmStation
        fields = "__all__"
