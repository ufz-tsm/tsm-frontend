from django import forms

from tsm.utils import get_time_with_unit_by_time_in_minutes


class SyncIntervalChoiceWidget(forms.MultiWidget):
    UNIT_CHOICES = (
        ("min", "Minutes"),
        ("h", "Hours"),
        ("d", "Days"),
    )

    def __init__(self, *args, **kwargs):
        widgets = (
            forms.NumberInput(),
            forms.Select(choices=self.UNIT_CHOICES),
        )
        super().__init__(widgets=widgets, **kwargs)

    def decompress(self, value):
        value, unit = get_time_with_unit_by_time_in_minutes(value)

        return value, unit if value and unit else 15, "min"
