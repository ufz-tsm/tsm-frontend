from django import forms


class ThingDatastreamTupleSelectorWidget(forms.MultiWidget):

    choices = []

    def __init__(self, *args, **kwargs):
        widgets = (
            forms.SelectMultiple(choices=[]),
            forms.SelectMultiple(choices=self.choices),
        )
        super().__init__(widgets=widgets, **kwargs)

    def decompress(self, value):
        if not value:
            return None, None
        return value, value
