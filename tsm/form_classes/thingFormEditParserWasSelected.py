from django import forms

from tsm.form_classes.groupReadOnlyField import GroupReadOnlyField
from tsm.form_classes.syncIntervalChoiceField import SyncIntervalChoiceField
from tsm.form_classes.parserChoiceFieldThingEditForm import (
    ParserChoiceFieldThingEditForm,
)
from tsm.models import ParserType, Thing
from tsm.utils import custom_clean


class ThingFormEditParserWasSelected(forms.ModelForm):
    parser_type = forms.ModelChoiceField(
        queryset=ParserType.objects.all(), label="Select parser type", required=False
    )
    parser = ParserChoiceFieldThingEditForm(
        queryset=None, label="Select parser", required=False
    )
    group = GroupReadOnlyField()
    ext_sftp_sync_enabled = forms.BooleanField(
        label="Enable fileserver sync", initial=True, required=False
    )
    ext_sftp_sync_interval = SyncIntervalChoiceField(required=False)

    class Meta:
        model = Thing
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        instance: Thing = kwargs.get("instance")
        initial = kwargs.get("initial", {})
        if instance:
            first_parser = instance.parser.first()
            if first_parser:
                initial["parser_type"] = first_parser.type.id
                initial["parser"] = first_parser.pk
        kwargs["initial"] = initial
        super().__init__(
            *args, **kwargs
        )  # must be called so that the ['inital'] values get set.

    def clean(self):
        cleaned_data = super().clean()
        custom_clean(self, cleaned_data)
