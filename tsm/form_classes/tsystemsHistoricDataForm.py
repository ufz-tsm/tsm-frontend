from django import forms
from datetime import datetime, timezone


class TSystemsHistoricDataForm(forms.Form):
    begin_date = forms.DateTimeField(
        label="Begin date",
        help_text="Datetime format: YYYY-MM-DD HH:MM:SS",
        required=True,
    )
    end_date = forms.DateTimeField(
        label="End date",
        help_text="Datetime format: YYYY-MM-DD HH:MM:SS",
        required=True,
    )

    def clean(self):
        cleaned_data = super().clean()
        begin_date = cleaned_data.get("begin_date")
        end_date = cleaned_data.get("end_date")

        if begin_date and end_date:
            current = datetime.now(timezone.utc)

            if begin_date > current:
                self.add_error("begin_date", "The begin date must be in the past.")
            if end_date > current:
                self.add_error("end_date", "The end date must be in the past.")

            if begin_date >= end_date:
                self.add_error("end_date", "The end date must be after the start date.")

        return cleaned_data
