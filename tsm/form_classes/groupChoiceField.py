from django import forms
from django.contrib.auth.models import Group

from tsm.utils import get_group_name_with_vo_prefix, filter_group_queryset_in_allowed_vo


class GroupChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return get_group_name_with_vo_prefix(obj)

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop("queryset", Group.objects.none())
        super().__init__(*args, queryset=queryset, **kwargs)
        self.queryset = self.get_filtered_queryset()

    def get_filtered_queryset(self):
        queryset = self.queryset.all()  # Retrieve all groups initially

        queryset = filter_group_queryset_in_allowed_vo(queryset)

        # only groups with '#' in the name
        queryset = queryset.filter(name__contains="#")
        # only groups which have users
        queryset = queryset.filter(user__isnull=False).distinct()
        # only groups with a name, when split by ":" consists of six parts
        # will include:
        # urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:Testcommunity#login-dev.helmholtz.de
        # will not include:
        # urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:Testcommunity:ufz-sms-admin#login-dev.helmholtz.de
        queryset = queryset.filter(name__regex=r"^[^:]+(:[^:]+){5}$")
        return queryset
