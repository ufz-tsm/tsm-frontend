from django import forms
from django.contrib import admin

from tsm.form_classes.timestampColumnForm import TimestampColumnFormSet
from tsm.models import CsvParser, TimestampColumn


class CsvParserFormUser(forms.ModelForm):

    class Meta:
        model = CsvParser
        exclude = ["pandas_read_csv", "type"]
