from django import forms
from django.contrib.auth.models import Group

from tsm.utils import get_group_name_with_vo_prefix


class GroupReadOnlyWidget(forms.MultiWidget):
    def __init__(self, **kwargs):
        widgets = {
            "text": forms.Textarea(attrs={"readonly": "readonly", "rows": 2}),
            "": forms.HiddenInput(),
        }

        super().__init__(widgets=widgets, **kwargs)

    def decompress(self, value):
        if value:
            group = Group.objects.get(pk=value)
            group_name = get_group_name_with_vo_prefix(group)
            return [group_name, value]
        return [None, None]
