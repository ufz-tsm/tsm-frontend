from django import forms

from tsm.form_classes.qaqcFunctionForm import QaqcFunctionForm
from tsm.models.qaqc_functions.FreeTextFunction import FreeTextFunction


class FreeTextFunctionForm(QaqcFunctionForm):
    function = forms.CharField(
        widget=forms.Textarea,
        label="Function",
        required=False,
        help_text="Write your own function. You can refer to selected datastreams via their aliases.",
    )

    def get_required_fields(self):
        return ["function"]

    class Meta:
        model = FreeTextFunction
        fields = "__all__"
