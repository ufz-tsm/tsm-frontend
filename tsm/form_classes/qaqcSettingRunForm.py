from django import forms


class QaqcSettingRunForm(forms.Form):
    begin_date = forms.DateTimeField(
        label="Begin date", help_text="Datetime format: YYYY-MM-DD HH:MM:SS"
    )
    end_date = forms.DateTimeField(
        label="End date", help_text="Datetime format: YYYY-MM-DD HH:MM:SS"
    )

    def clean(self):
        cleaned_data = super().clean()
        begin_date = cleaned_data.get("begin_date")
        end_date = cleaned_data.get("end_date")

        if begin_date and end_date:
            if begin_date >= end_date:
                self.add_error("end_date", "The end date must be after the start date.")

        return cleaned_data
