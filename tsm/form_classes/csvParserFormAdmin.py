from django import forms

from tsm.models import CsvParser


class CsvParserFormAdmin(forms.ModelForm):

    class Meta:
        model = CsvParser
        fields = [
            "group",
            "name",
            "delimiter",
            "exclude_headlines",
            "exclude_footlines",
            "pandas_read_csv",
        ]

    def clean(self):
        cleaned_data = super().clean()
        # validate json field
        obj = cleaned_data.get("pandas_read_csv")
        if obj is not None and not isinstance(obj, dict):
            self.add_error(
                "pandas_read_csv", f"Expected a JSON object (curly braces) or 'null'."
            )
