from rest_framework import serializers
from tsm.models import Thing, Parser
from django.contrib.auth.models import Group


class ParserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parser
        fields = "__all__"


class ThingSerializer(serializers.HyperlinkedModelSerializer):
    # parser = ParserSerializer(many=True)
    class Meta:
        model = Thing
        fields = ["name", "thing_id", "description", "group"]


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ["id", "name"]
