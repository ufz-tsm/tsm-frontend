# Getting Started

## 1. Create environment/config file from example:

```bash
cp .env.example .env
```
- Update the `DJANGO_HELMHOLTZ_CLIENT_SECRET` to the correct secret

## 2. Run the services
- `docker compose up -d`
- visit: `localhost`
  - landing page, with one service `frontend` (redirects to `localhost/frontend` )
- visit: `localhost/frontend` 

# Development

## Run black formatter
- check: `docker run --rm --volume $(pwd):/src --workdir /src pyfound/black:latest_release black --check .`
- format: `docker run --rm --volume $(pwd):/src --workdir /src pyfound/black:latest_release black .
`

## Run tests
- `docker compose exec frontend python3 manage.py test` on running container
- or `docker compose run frontend python3 manage.py test` (could take a few seconds if containers are not running yet)
- to run a specific test (or collection of tests):
  - note: the following examples use the first option, it will also work with `docker compose run frontend...`  
  - one test methode of one testcase:
    - `docker compose exec frontend python3 manage.py test tsm.tests.test_forms.ThingFormTest.test_valid_payloads`
  - one testcase:
    - `docker compose exec frontend python3 manage.py test tsm.tests.test_forms.ThingFormTest`
  - collection of testcases:
    - `docker compose exec frontend python3 manage.py test tsm.tests.test_forms`
# Resources
- https://docs.djangoproject.com/en/4.0/
- https://www.django-rest-framework.org/

## Django AdminSite-API and URL patterns
- https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#reversing-admin-urls
- https://docs.djangoproject.com/en/3.2/_modules/django/contrib/admin/sites/

## alternate Django theme
- https://github.com/fabiocaccamo/django-admin-interface
  - Um weiterhin Templates selbst anpassen zu können: https://github.com/bittner/django-apptemplates
  - an die default django themes kommt man z.B. so heran: ``docker cp tsm-frontend-frontend-1:/usr/local/lib/python3.10/site-packages/django/contrib/admin/templates/admin/base_site.html .`` 

# Keycloak as identity provider in development environment

## Admin console

- http://localhost:8080/admin/master/console/#/demo/ 
  - or: http://keycloak:8080/admin/master/console/#/demo/ (if you updated /etc/hosts)
  - or: http://keycloak:KEYCLOAK_PORT/admin/master/console/#/demo/ || http://localhost:KEYCLOAK_PORT/admin/master/console/#/demo/ (if you changed the port to something other than 8080) 
- Credentials:
  - User: `admin`
  - Password: `admin`

See [here](./keycloak/README.md) for further information regarding configuration and setup.

# Regular user to authenticate on Django

User: `myuser`
Pass: `foo`

## Exporting realm as fixture for dev env

To save new users, groups or changes in the Keycloak config to the initial setup fixtures (i.e. 
when starting a fresh docker-compose environment after `docker-compose down -v`) its necessary 
to export them to the init file:

```bash
docker compose exec keycloak /opt/keycloak/bin/kc.sh export --file /opt/keycloak/data/import/keycloak-init.json --users same_file --realm keycloak-demo-tsm
```

If you have write permissions issues check if `UID` and `GID` environment variables are set and 
match the ownership of the local `keycloak/keycloak-init.json` file. Unfortunately you have to 
recreate the container for changes of the `UID` and `GID` where you will loose changes in keycloak. 

## How to add custom authorisation group without using the Helmholtz Virtual Organization

To add a custom authorisation group without using the Helmholtz Virtual Organization you need to add a group with the same naming pattern as the groups of the Helmholtz Virtual Organization.

The name of your group should follow this naming pattern: `urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:<my-fancy-group-name>#login-dev.helmholtz.de`

__Example with steps__:

`urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:MyTestGroup#login-dev.helmholtz.de`

1. login in as admin
1. add group via admin interface with name `urn:geant:helmholtz.de:group:UFZ-Timeseries-Management:MyTestGroup#login-dev.helmholtz.de`
   1. add permissions to crud (create read update delete)`thing` to that group
2. add that group to a user (to yourself) using the admin interface
3. Login in as normal user (using aai)
4. Create a thing -> you should be able to use the new group
